# CapScanner #
## Features ##

1. Report engine for a network packet captures (*.pcap and *pcapng formats)

2. Supported reports:

    2.1. DSN request and reponse

    2.2. Incoming ping requests

    2.3. TCP handshake  packets

    2.4. Outgoing traffic by destination (country, remote hostname)

    2.5. Incoming UDP packets by remote host and target port 

3. Reports in the following formats:

    3.1. console output

    3.2. comma-separated text (CSV)

    3.3. Excel-compatible files (XLSX)

    3.4. MySQL export with automatic alteration of tables

4.  Additional features:

    4.1. Automatically scans the specified folder for all files by wildcard * .pcap*.

    4.2. Nested scan in subfolders, if any

    4.3. Automatic names for reports

## Usage: ##

Note. Use appropriate version of script for version of Wireshark (which is used for capturing and for post-processing of the captures):

+ Wireshark 1.10 - capscanner.py
+ Wireshark 1.12 - capscanner.1.12.py

(These version differs in names of some fields, e.g., DNS response).

+ On Linux: 
	capscanner.py [arguments]
+ On Windows:
 	python capscanner.py [arguments]

*Note. All arguments in the named format like: <argument name>=<value>. If value contains spaces (for example, some folders in the path have spaces in their names), please use surrounding double quotas. In other case, surrounding quotas are not necessary.*

The better way to specify arguments is to use "settings.py" for that purpose.

This engine uses a relative path for its' internal components, so please ensure that the current folder when script is launched - is the same where script is located (that can be importsnt if you launch engine from other sh/bat file or by a system shortcut).

### Arguments: ###

Format:

  *<name>=<value>* (please avoid spaces in values, otherwise use a quoted strings).

1. **scanfolder** (default value: './samples') - folder with input PCAP and PCAPNG files (this is not a full path to the file, but path of the folder). Nested folders will be scanned also.
2. **outformat** (default value:  'csv') - format of reports. possible values: 'console', 'csv', 'excel', 'sql'.
3. **localnet** (default value: '10.0.0.0/16') - Local network. This parameter uses splash (CIDR) syntax for ip subnet definition and means the pool of local ip adresses to define local destinations and local traffic, for example: localnet=192.168.0.0/16. 

4. **dbconnstr** (default value:  'mysql+pymysql://usrpcap:passwd@127.0.0.1/pcap') - connection string to MySQL server
5. **lkp_geoip** kwargs(default value: './res/dbip-country.csv') - path to Geo IP file to import into engine.
6. **lkp_iana_protocols** (default value:  './res/protocol-numbers-1.csv') - path to list of IANA registered IP-protocols

7. **verbose** (default value: 1) - if non-zero, perform aditional warnings on the screen. You can swith it off when you choose 'console' output for reports.
8. **doctest** (default value: 0) - Development. You can run internal test suites.
9. **ip6compat** (default value:  0) - IP6 compatibility mode. Do not change this value.
10. **interval** (default value: 0) - interval between report updates, in seconds. When interval >0, script running in the observing mode. Run "stop_observer.py" to close the observer process. 

Note. You can specify all these arguments in a file "settings.py" (in the same folder where script is located).

## Requirements ##

1. **Python 2.7.x**
2. **Wireshark 1.10-1.12**
3. Python **Steelscript** package
4. Python **Pandas** package

### Optional dependencies: ###

5. **SQLAlchemy** and **pymysql** (to export results in **MySQL**)
6. **xlsxwriter** package (to export results in *.xlsx format)

## Installation and usage ##

1. Install Riverbeds steelscript by: "pip install steelscript" (or "sudo pip install.." if you on Linux)
2. Go to <python folder>/scripts/ and run "steel install" (instructions from Riverbeds' site)
3. install Wireshark wrapper by: "pip install steelscript.wireshark"
4. Install "pandas" package (if not installed) by "pip install pandas"
5. Download the latest version of the script from Bitbucket repository.
6. Ensure that the wireshark an tshark are in your PATH environment so you can call them like ">tshark".
7. Make a several test captures with Wireshark and save them in any folder.
8. Run the main script: "python capscanner.py outformat='csv' scanfolder='./samples' ". Possible values of "outformat" are: console | csv | excel | sql; 

### Report output ###

Each individual report gets an unique name, based on the following template:

	<start date-time>_<end time>_<view name><optional suffix>.<type extension>. 

Start time and end time are timestamp in a local format with a microseconds postfix, all separator charactrs like space, period, dash, will be removed. View name depends on report meaning: 'dnsreport', 'tcphandshake', 'ping', 'country_breakdown', 'udp_flow'. Extension corresponds to the selected output format (".csv", ".xlsx").

## Updating Geo IP database ##

Note. The current version contains the latest database of ip addresses by country (up to September 2015). If you want to update this database later, please follow to instructions below.

1. Download the latest version of database from "https://db-ip.com/db/#downloads" (select database type "IP address to country", CSV format). 
2. Change the current folder to the folder where script is located.
3. Change the current folder to the internal "res" folder.
4. Unzip file, extract the wrapped CSV file to the same folder where script is located.
5. Remove 'iplookup.pickle' file from the engines' folder.
6. Run the script. It will automatically update the internal database. Look on messages on the console. After sucessfull import, you can remove the uploaded CSV file.

## Updating IP codes registry ##

1. You can edit the delivered file "./res/protocol-numbers-1.csv" to update the latest changes in the protocols registry, if necessary.
2. Remove "ip_protocols.pickle" file from engines' folder.
3. Run the script. It will automatically update the internal database. Look on messages on the console. After sucessfull import, you can remove the uploaded CSV file.

## Exporting reports to MySQL database ##

1. This feature requires:

    1.1. Installed SQLAlchemy package ("pip install sqlalchemy")

    1.2. Installed "pymysql" package ("pip install pymysql")

    1.3. Access to MySQL server, user account for connection, and database to store reports

    1.4. Set an actual configuration in the file "settings.py" (located in the same folder with a script), in the following format: "mysql+pymysql://<username>:<password>@<host>/<database>". Example settings: *mysql+pymysql://usrpcap:passwd@127.0.0.1/pcap.*

    1.5. Run the script with option *outformat=sql*

2. Field names correspons to the same names for file-based reports, except that the dots in the name are replaced by underscore characters. You can run the test output to database to see the structure of tables.

3. Field types for columns:

	* dns_id:Text
	* dns_qry_name: Text
	* dns_resp_addr:Text
	* frame_len:Integer
	* frame_number: BigInteger
	* frame_time: DateTime
	* ip_dst: Text
	* ip_proto: SmallInteger
	* ip_src: Text
	* ip_ttl: SmallInteger
	* tcp_dstport: Integer
	* tcp_stream: Text
	* udp_dstport: Integer
	* udp_srcport:Integer

4. Each report will be in separate table (see "Report output" for namint rules, except that the each table will be also prefixed by "t_"). 

Note. Do not create individual tables for each report manually. Tables will be created (or altered) automatically by the script. 

## Additional notes ##

When you choose "excel" format for reports, be sure, please, that the previous versions of the same reports are not opened in Excel at the same time when you running the script. Excel uses file locking, so it will prevents to overwrite files (the scrpt will display an error).


## Changelog ##

### 0.9.0 ###

1. Improved TcpHandshare report (faster).
2. Mutiple reports for view is supported (used in PingView, which supports details and aggregated report).
3. More reliable gateway to XLS files (checks installed driver explicitly upon startup).
4. Improved error handling.

## Roadmap ##

* Web-interface for engine configuration.
* Web-interface for a breef analysis of reports.
* Charts/diagrams.
* IPv6 support (for DNS lookup, Geo-IP).
* Additional *unittest* suite.

