#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# if "env" fails, change the first line to: #!/usr/bin/python

# substitute statement by build-in function
from __future__ import print_function

__VER__ = (0,9,10)

"""
Requirements:
Wireshark version 1.12
You have tshark installed locally
Hint: it is the best to make capture neither on observed client nor on observed server.
"""

_lookups = None

# ###########################################
# IMPORT
# ###########################################

import os
import sys
import time

print(sys.version)

from pcapmon import\
    emessages as _msg,\
    errors as mdErrors,\
    env as mdEnv,\
    monitor,\
    core as _core,\
    views as views

from pcapmon.lookup import\
    IPTree,\
    IPRanges,\
    GeoIPLookup,\
    DNSCache,\
    ProtocolNamesLookup,\
    CountryNameLookup

warning = monitor.warning

# from pcapmon.pcap_fixed import PcapFile
# import pcapmon.rptbackup as rptbackup

# ###########################################
# Main()
# ###########################################

def init_engine():
    """
    Init environment
    """
    global _lookups

    try:
        settings = mdEnv.SettingsModule('settings')
    except mdErrors.EEnvError:
        # create default environment
        warning('Settings module not found, proceeding...')
        settings = mdEnv.Settings()

    # extend environment by command-line arguments:
    settings.updatefrom(mdEnv.CLI())

    ENV = mdEnv.Environment(**settings.vars)
    mdEnv.set(ENV)

    if 'doctest' in dir(ENV) and ENV.doctest:
        monitor.VERBOSE = False # <-- suppress of warnings which output can violates doctest ethalon output

        # add test folder to search modules
        path = './test'
        if path not in sys.path: sys.path.append(path)

        import doctest
        doctest.testmod(verbose=ENV.verbose)
        sys.exit(0)

    monitor.VERBOSE = ENV.verbose

    _lookups=dict(
        iplookup=GeoIPLookup(label='Geo IP', csv_name=ENV.lkp_geoip, pickle_name='iplookup.pickle'),
        country_names = CountryNameLookup(label='Country Name by Code', csv_name=ENV.lkp_country_names, pickle_name='countryname.pickle'),
        protolookup = ProtocolNamesLookup(label='IANA IP protocols registry', csv_name=ENV.lkp_iana_protocols, pickle_name='ip_protocols.pickle'),
        dnscache = DNSCache(pickle_name='dns.pickle')
    )


def run_once():
    """
    Single pass for reports
    """
    global _lookups

    ENV = mdEnv.get()

    t_start = time.time()

    # DnsView must be executed at the beginning
    # to update a local DNS cache.
    scanner = _core.PCapAnalyzer(
        [
            views.DnsView('dnsreport', _lookups),
        ],
        env = ENV)
    scanner.appendfolder(ENV.scanfolder)
    scanner.run(rename=False)

    # update new dns cache:
    _lookups['dnscache'].store()
    # log dns cache:
    # _lookups['dnscache'].to_csv('./dns.csv')
    
    # run other reports, do not rename source files
    scanner = _core.PCapAnalyzer(
        [
            views.PingView('ping'),
            views.TCPHandshakeView('tcphandshake'),
            views.TrafficToCountriesViewExtended('country_breakdown', _lookups),
            views.TorrentView('torrent', _lookups),
            views.UDPView('udp_flow'),
            views.ProtocolsCountByPeers('protocols_view_summary', _lookups),
            views.DnsCountByPeers('dns_queries_summary', _lookups),
            views.ViewInboundTrafficByPeers('inbound_traffic_summary', _lookups),
            views.ViewOutboundTrafficByPeers('outbound_traffic_summary', _lookups)
        ],
        env = ENV)
    scanner.appendfolder(ENV.scanfolder)
    scanner.run(rename=False)

    # run last report, rename source files
    scanner = _core.PCapAnalyzer(
        [
            views.CounterView('counters', processing_time=(time.time() - t_start) / 60),
        ],
        env = ENV)
    scanner.appendfolder(ENV.scanfolder)
    scanner.run(rename=True)

def observe():
    ENV = mdEnv.get()
    flag_file = os.path.join(ENV.script_folder, 'observing.state')
    
    try:
        warning('Starting observer with interval {} seconds...'.format(ENV.interval))
        # create flag file:
        with open(flag_file, 'wb') as f:
            f.write('service is running')
        # run loop while file exists
        while os.path.exists(flag_file):
            warning('--------------------------------')
            warning('New pass, time is: {!r}...'.format(time.strftime('%X')))
            run_once()
            time.sleep(ENV.interval)
    finally:
        if os.path.exists(flag_file):
            os.remove(flag_file)
        
def main():
    # init engine
    init_engine()
    # get environment
    ENV = mdEnv.get()

    if ENV.verbose:
        print('Starting with settings:\n')
        print(str(ENV), '\n')

    if ENV.interval > 0:
        observe()
    else:
        run_once()


if __name__ == '__main__':
    main()







