import os, sys
local_folder = os.path.split(sys.argv[0])[0]
flag_file = os.path.join(local_folder, 'observing.state')
if os.path.exists(flag_file):
	os.remove(flag_file)
	print 'Sending "STOP" command... \nScript will be stopped after the finishing of all  currently running reports \nfrom the internal task queue.\nDone.'
else:
	print 'Stop command has been sent.'