#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# if "env" fails, change the first line to: #!/usr/bin/python
# substitute statement by build-in function
from __future__ import print_function

__VER__ = (0,9,10)

import sys

from pcapmon.dbbackup import DatabaseBackup

if __name__ == '__main__':
    #~ import doctest
    #~ doctest.testmod()
    def hint():
        print('Script usage: \n\n'+
            '1.rptbackup enum <backup root> - enumerates all available backups as table:\n'+
            '<backup index> <backup timestamp of last modification>\n\n'+
            '2. rptbackup restore <backup root> <backup index> - restore shapshot with index=<index>\n\n'+
            'Note: For the Windows path, use double slashes instead of a single one or a direct shash "/".\n'+
            'To configure database connection, set the appropriate "dbconnstr" in "settings.py"')
    
    import sys
    args = sys.argv[1:]
    if len(args) < 2:
        hint()
        sys.exit(0)
    if args[0] == 'enum':
        dbb = DatabaseBackup(args[1])
        for index in range(0, len(dbb.partitions)):
            print('\nIndex\tTimestamp\n')
            print(index, '\t', dbb.get_partition_info(index))
        sys.exit(0)
    if args[0] == 'restore':
        if len(args) < 3:
            hint()
            sys.exit(0)
        dbb = DatabaseBackup(args[1])
        dbb.select_partition(int(args[2]))
        import sqlalchemy as _SQA
        dbb.restore_all(_SQA.create_engine(dbconnstr))
        print('Done.')
