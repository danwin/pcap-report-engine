# ###########################################
# IMPORT
# ###########################################

import os
import sys
import time
import datetime

from pcapmon import \
    emessages as _msg,\
    errors as mdErrors,\
    env as mdEnv,\
    dbi as dbi,\
    dbbackup,\
    monitor

_getEnv = mdEnv.get

warning = monitor.warning
TRACE_EXCEPTIONS = monitor.TRACE_EXCEPTIONS

from pcapmon.pcap_fixed import PcapFile


def exit(msg):
    print msg
    sys.exit(1)

try:
    import steelscript as _sts
except ImportError:
    exit (_msg.STEELSCRIPT_IMPORT_ERROR)

try:
    import steelscript.wireshark as _wshk
except ImportError:
    exit (_msg.STEELSCRIPT_WIRESHARK_IMPORT_ERROR)

try:
    import pandas
except ImportError:
    exit (_msg.PANDAS_IMPORT_ERROR)

try:
    import cPicle as pickle
except ImportError:
    import pickle


# ###########################################
# DB tools (requires SQLAlchemy / pymysql)
# or works in a "stub" mode for file storages
# ###########################################

from pcapmon.dbi import \
    DB_ENABLED,\
    TableFromModel,\
    connectdb as _connectdb,\
    tofieldname as _t,\
    is_pcap_in_db,\
    count_table_length,\
    truncate_table

# ###########################################
# date/time transform
# ###########################################

def check_dt_type(value):
    if not isinstance(value, datetime.datetime):
        raise mdErrors.ECScannerError( \
            'check_dt_type() error: '+\
            'value has type {} instead of "datetime".'\
            .format(type(value)))
    return value

_epoch = datetime.datetime(year=2000,month=1,day=1,hour=0,minute=0,second=0)

def to_time_reference(dt, zero=_epoch):
    """
    Return integer value in microresonds from _epoch (1/1/2000):
    >>> d0 = _epoch
    >>> to_time_reference(d0)
    0
    >>> d1 = datetime.datetime(year=2000, month=1, day=2)
    >>> to_time_reference(d1) == 1 * 24 * 3600
    True
    """
    tdelta = (check_dt_type(dt) - _epoch)
    return tdelta.microseconds + \
        (tdelta.seconds + tdelta.days * 24 * 3600) * 10 ** 6

# ###########################################
# Pandas helpers for DataFrame:
# ###########################################

def df_select_cols(df, cols):
    """
    Select only fileds enumerated in "cols"
    """
    try:
        return df.loc[:, cols]
    except KeyError as e:
        missed = set(cols) - set(df.columns.values.tolist())
        txt_missed = ', '.join(list(missed))
        raise mdErrors.EReportError( \
            'Cannot select required fields in report: missed fields are: {}'.format(txt_missed))

def df_restrict_columns(df, cols):
    """
    Remove extra columns in DataFrame which are not in list
    """
    acols = set(df.columns.values.tolist())
    cols = set(cols)
    extra = acols - cols
    for name in extra:
        del df[extra]

#~ def df_create_hash_view(df):
    #~ """
    #~ Spawn DataFrame view with a new computed hash key.
    #~ Hash includes enumerate~d fields.
    #~ """
    #~ if df.index.name == 'hashkey':
        #~ df = df.reset_index()
    #~ return df.drop_duplicates(subset='hashkey').set_index('hashkey')

def df_apply_source_info(df, view):
    """
    Create 2 columns to define time range of capture
    and the source pcap file.
    (require PCapView protocol)
    """

    if len(df.index) > 0:
        starttime, endtime = \
            check_dt_type(view.starttime), check_dt_type(view.endtime)

        df['pcap'] = view.fpcapname
        df['pcap_first_timestamp'] = to_time_reference(starttime)
        df['pcap_last_timestamp'] = to_time_reference(endtime)

def df_force_integer(v):
    """
    Try to convert value to integer.
    Supports input:
    pandas/numpy int32;
    decimal str
    hex str
    python int/long
    """
    try:
        return v.astype(int32)
    except:
        if isinstance(v, str):
            try:
                return int(v, 16)
            except ValueError:
                try:
                    return int(v, 10)
                except:
                    raise Exception('Error in view: cannot convert "integer" field to integer: '+type(v))
        else:
            return v

# ###########################################
# Report()
# ###########################################


# ###########################################
# Report
# ###########################################

class Report(object):

    @staticmethod
    def safename(name):
        return name.lower().translate(None, ':/ \\+-?.')

    def __init__(self,
            parentview,
            outformat=None,
            namesuffix=None,
            data=None,
            db_model=None,
            tbname_maxlen = 64,
            mode = 'append_records'
        ):
        """
        >>> view = PCapView(rptfname='View+name'*2)
        >>> r = Report(view, namesuffix='namesuffix', tbname_maxlen=10)
        >>> r.tablename

        >>> r.filename

        >>> view = PCapView(rptfname='View+name')

        >>> r = Report(view)
        >>> r.tablename

        """
        starttime, endtime = parentview.starttime, parentview.endtime

        self.df = data

        self.db_model = db_model

        self.tbname_maxlen = tbname_maxlen

        outformat = outformat or _getEnv().outformat or 'console'
        sformats = ('console', 'csv', 'excel', 'sql')
        if outformat not in sformats:
            raise errors.EStorageError( \
                'Invalid storage type "{}" for report. Valid values are: {}' \
                .format(outformat, ', '.join(sformats)))
        if outformat == 'excel' and not xlsxwriter:
            raise errors.EStorageError('Your system does not support XLSX output. Install xlsxwriter package.')
        self.outformat = outformat
        # panda dataframe:

        viewname = self.safename(parentview.rptfname)
        #compute source hash:
        self.srchash = hash((viewname, starttime, endtime))

        namesuffix = self.safename(namesuffix) if namesuffix is not None else None
        if namesuffix:
            viewname = '_'.join([viewname, namesuffix])

        # name for db_table:
        self.tablename = viewname
        # MySQL limits the name of table to 64 chars
        if len(self.tablename) > self.tbname_maxlen:
            self.tablename = self.tablename[:self.tbname_maxlen]

        _nametags = []

        if starttime is not None and endtime is not None:
            _nametags.append("_".join([starttime.strftime('%y_%m_%d_%H%M%S'), endtime.strftime('_%H%M%S')]))

        _nametags.append(viewname)

        fpcapname = parentview.fpcapname
        if fpcapname:
            _nametags.append(os.path.split(fpcapname)[1])

        self.filename = '_'.join(_nametags)

        # validate that report name is assigned
        if outformat == 'console':
            pass
        elif outformat == 'sql':
            if not self.tablename:
                raise errors.EStorageError('tablename for DB report is not assigned!')
        else:
            if not self.filename:
                raise errors.EStorageError('viewname for report is not assigned!')

        self.view = parentview

        warning('Report "{}" computed: {} rows'.format(viewname, len(data.index)))
        
        rc_modes = ('append_records', 'update_records')
        if mode in rc_modes:
            self._mode = 'append_records' #mode
        else:
            raise errors.EReportError('Invalid mode value: {}.'.format(mode))

    def __del__(self):
        """
        Remove circular reference~s
        """
        self.df = None
        self.view = None

    def dbschema(self):
        return None

    def _save_console(self):
        print(self.df)
        return True

    def _save_csv(self):
        self.df.to_csv(os.path.join(_getEnv().outpath, self.filename+'.csv'))
        return True

    def _save_excel(self):
        if xlsxwriter is None:
            raise errors.EStorageError('Cannot write "xlsx" files: xlsxwriter is not installed!')
        self.df.to_excel(os.path.join(_getEnv().outpath, self.filename+'.xlsx'))
        return True

    def _save_sql(self):
        if not DB_ENABLED:
            raise errors.EStorageError('Your environment does not support database operations!')

        # make "safe" names of columns, without dotted notation:
        self.df.rename(columns=_t, inplace=True)

        engine = _connectdb()
        db_backup = dbbackup.getinstance()

        _view = self.view
        if self._mode == 'append_records' and is_pcap_in_db(
                engine,
                self.tablename,
                _view.fpcapname,
                to_time_reference(_view.starttime),
                to_time_reference(_view.endtime)):
            warning('Report is already in DB')
            return False

        # alter DB table~s if not exist:
        TableFromModel(engine, self.tablename, self.db_model, rewrite=(self._mode !='append_records'))

        try:
            self.df.to_sql(
                self.tablename,
                engine,
                #~ schema=self.dbschema(),
                # data type for each column:
                #~ dtype = self.dbtypes,
                # use index for DB column:
                index=False,
                # use index name from DataFrame:
                index_label=None,
                # append mode, create table if doesn't exist:
                if_exists='append' if self._mode == 'append_records' else 'rewrite', # <- means pandas behavior - do not alter table structure if exists one
                chunksize=1000)
        except Exception as e:
            if TRACE_EXCEPTIONS: raise
            raise errors.EStorageError('Error on writing report "{}" to database: \n{!s}'.format(self.tablename, e))
        # create backup:
        warning('Backup: {}'.format(self.tablename))
        db_backup.store(self.df, self.tablename)
        # count maximal table length:
        db_backup.db_max_length = max(db_backup.db_max_length, count_table_length(engine, self.tablename))
        return True

    def truncate_table(self):
        if not DB_ENABLED:
            return
        truncate_table(_connectdb(), self.tablename)

    def save(self):
        if self.df is None:
            warning('Report is empty.')
            return
        handler = getattr(self,  '_save_' + self.outformat)
        if not handler:
            raise errors.EStorageError('Invalid format for report: {}'.format())
        if handler():
            warning('{} rows saved.'.format(len(self.df.index)))
        else:
            warning("Report is up-to-date.")

# ###########################################
# CounterReport
# ###########################################

class CounterReport(Report):
    """
    Report with incremental values,
    the new dataframe used as increment
    """
    def __init__(self,
            parentview,
            outformat=None,
            namesuffix=None,
            data=None,
            db_model=None,
            tbname_maxlen = 64
            ):
        Report.__init__(self, parentview, outformat, namesuffix, data, \
            db_model, tbname_maxlen, mode = 'update_records')
    
    def _save_sql(self):
        try:
            # load previous data and compute sum
            saved_data = pandas.read_sql_table(self.tablename, _connectdb(), schema=None, index_col=None, coerce_float=True, parse_dates=None, columns=None, chunksize=None)
            # to-do: populate counters with zeros
            self.truncate_table()

            # convert names to the common format:
            self.df.rename(columns=_t, inplace=True)

            self.df = saved_data.add(self.df, fill_value=0)
            warning('Updating counter table "{}" in DB'.format(self.tablename))
        except Exception as e:
            warning('Table "{}" is empty, creating new table in DB'.format(self.tablename))
            if TRACE_EXCEPTIONS: 
                print('Data update: ', e)
        return Report._save_sql(self)        

# ###########################################
# Report with aggregation by keys: MultirowCounterReport
# ###########################################

class MultirowCounterReport(Report):
    """
    Report with incremental values,
    the new dataframe used as increment
    """
    def __init__(self,
            parentview,
            outformat=None,
            namesuffix=None,
            data=None,
            db_model=None,
            tbname_maxlen = 64,
            group_cols=None
            ):
        self.group_cols = map(_t, group_cols) # rename names to DB format 
        Report.__init__(self, parentview, outformat, namesuffix, data, \
            db_model, tbname_maxlen, mode = 'update_records')
    
    def _save_sql(self):
        try:
            # load previous data and compute sum
            saved_data = pandas.read_sql_table(self.tablename, _connectdb(), schema=None, index_col=None, coerce_float=True, parse_dates=None, columns=None, chunksize=None)
            self.truncate_table()
            #
            # convert names to the common format:
            self.df.rename(columns=_t, inplace=True)

            del saved_data['rec_id']
            self.df = pandas.concat([saved_data, self.df]).groupby(self.group_cols).sum().reset_index()
            #
            warning('Updating counter table "{}" in DB'.format(self.tablename))
        except Exception as e:
            warning('Table "{}" is empty, creating new table in DB'.format(self.tablename))
            if TRACE_EXCEPTIONS: 
                print('Data update: {!r}'.format(e))
        return Report._save_sql(self)

# ###########################################
# Views: PCapView() as a  base class
# ###########################################

class PCapView(object):
    """
    Tshark proxy object.
    Solves task:
    1. Compile query to tshark.
    2. Receive and interprete the result of query
    3. Transform / process data and store these results in a children Report instances
    4. Perform bulk save of Report~s
    Has a same meaning like DB "view".
    """

    def qparams(self):
        """
        Create arguments to format the query to tshark:
        1. self.filterexpr = "", with optional named placeholders for parametric queries (if "params" defined)
        2. self.params = {'param name':<default value>, ...}
        3. self.fieldnames = ['tshark.field1', ...], aquired fields from pcap data
        """
        raise NotImplemented('qparams must be overriden!')

    def rmodel(self):
        """
        Define model for report(s) output.
        Return dict where keys are names of models (default is "main")
        Create data definition to store default report in DB via SQLAlchemy, when 'sql' output format is selected.
        """
        raise NotImplemented('rmodel must be overriden!')

    def db_model(self, engine, tablename):
        """
        Allows to create SQLAlchemy table
        for main report of the view.
        Called by Report object when 'sql' output selected.
        Provide an individual model for
        each additional report which is created
        in processdata() explicitly (via Report() argument).

        Return SQLAlchemy Table instance
        """
        pass

    def __init__(self, rptfname=None, lookups=None):
        # setup class params for query:
        self.qparams()
        # setup report definition(s):
        self.rfields = self.rmodel()

        self.rptfname = rptfname or self.__class__.__name__.lower()
        self.lookups = lookups
        self.starttime = None
        self.endtime=None
        self.fpcapname = ''
        self.reports = []

        if hasattr(self, 'params') and self.params:
            args = {}
            for _paramname, pval in self.params.items():
                v = getattr(_getEnv(),_paramname, None)
                args[_paramname] = pval if v is None else v
            #~ print (self.params)
            self.filterexpr=self.filterexpr.format(**args)
            #~ print (self.filterexpr)

    def applyPCapObj(self, pobj):
        # clear previous reports:
        for report in self.reports:
            del report
        self.reports = []

        # update pobj statistics if necessary
        pobj.info()
        self.starttime = pobj.starttime
        self.endtime = pobj.endtime
        self.fpcapname = pobj.filename

        df = pobj.query(
                self.fieldnames,
                filterexpr=self.filterexpr,
                as_dataframe=True
                #~ , use_tshark_fields=False
                )

        if df is not None and len(df.index) > 0:
            warning('{} rows in query result'.format(len(df.index)))
            self.processdata(df)
        else:
            warning('no data for report in {} source'.format(self.fpcapname))


    def addreport(self, data=None, **kwargs):
        _report = Report(self, data=data, **kwargs)
        self.reports.append(_report)

    def processdata(self, data):
        """
        Method can be redefined in descendants,
        to process additional reports
        (aggregation, filtering, etc., ...)
        """
        self.addreport( \
                data=data, \
                db_model=self.rfields['main'] \
            )

    def save(self):
        for report in self.reports:
            try:
                report.save()
            except IOError as e:
                print (\
                    "Error writing {} - permission denied or file is opened in other application"\
                    .format(report.filename))
                print (e)
                if TRACE_EXCEPTIONS: raise


# ###########################################
# PCapAnalyzer()
# ###########################################

class PCapAnalyzer(object):

    def __init__(self, views, env=None):
        self.dataset = None
        self.sourcelist = []
        self.views = views or []
        self.env = env
        # maximal number of files to process at the same time:
        self.chunksize = env.chunksize

        if _getEnv().outformat == 'excel':
            try:
                import xlsxwriter
            except ImportError:
                warning (_msg.XLSWRITER_IMPPORT_ERROR)
                xlsxwriter = None

    def appendfile(self, fname, *args, **kwargs):
        if len(self.sourcelist) == self.chunksize:
            raise StopIteration()
        print fname
        self.sourcelist.append(PcapFile(fname))
        return len(self.sourcelist)

    def appendfolder(self, fname, *args, **kwargs):
        import os
        import fnmatch
        files_remain = 0
                
        def scan_pattern(rootpath, pattern):
            remains = 0
            for root, dirs, files in os.walk(rootpath):
                # iterate over files in alpha-sorted order:
                for filename in sorted(fnmatch.filter(files, pattern)):
                    if filename.split('.')[-1] == 'done':
                        continue
                    try:
                        self.appendfile(os.path.join(root, filename))
                    except StopIteration:
                        remains += 1
            return remains

        rootpath = fname
        files_remain += scan_pattern(rootpath, '*.pcap')
        files_remain += scan_pattern(rootpath, '*.pcapng')
        warning('Folder checked, files remaining for deferred processing: {}'.format(files_remain))

    def run(self, rename=False):
        env = _getEnv()
        db_backup = dbbackup.getinstance()
        # check maximal length of the longest db table:
        if env.outformat == 'sql':
            if db_backup.db_max_length > env.DB_MAX_TABLE_LEN:
                # clear tables:
                warning('Tables exceeds allowed limit, moving to the backup storage...')
                for view in self.views:
                    for report in view.reports:
                        report.truncate_table()
                        warning('Table truncated: {}'.format(report.tablename))
                db_backup.new_partition()
                # clear counter:
                db_backup.db_max_length = 0
        
        # build reports
        # sort capture~s by creation time:
        self.sourcelist.sort(lambda a, b: cmp(os.path.getctime(a.filename), os.path.getctime(b.filename)))

        #for source in self.sourcelist:
        while len(self.sourcelist):
            source = self.sourcelist.pop()
            for view in self.views:
                warning( \
                    'Starting report: {} for {}...'\
                    .format(view.__class__.__name__, source.filename))
                try:
                    view.applyPCapObj(source)
                    view.save()
                except Exception as e:
                    print ('Error: ', e)
                    if TRACE_EXCEPTIONS: raise
                    continue
                warning('Done.')
            if rename:
                fname = source.filename
                os.rename(fname, fname+'.done')            
                
        warning('Files processed: ', len(self.sourcelist))
        warning('Views per file: ', len(self.views))


