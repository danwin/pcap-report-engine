import os
import sys

from pcapmon import errors as mdErrors
from pcapmon.monitor import TRACE_EXCEPTIONS

_env = None

def get():
    return _env

def set(env):
    global _env
    _env = env
	
# ###########################################
# Environment()
# ###########################################

class Environment(object):
    """
    Placeholder for settings (a flat namespace for settings variables).
    Holds data model, with typecasting.
    Only specified attributes will be extracted from kwargs.
    """
    def __init__(self, **kwargs):
        if kwargs is None:
            kwargs = {}
        self._buffer = None
        try:
            self._buffer = dict(
                ip6compat = int(kwargs.get('ip6compat', 0)),
                outformat = kwargs.get('outformat', 'csv'),
                outpath = kwargs.get('outpath', '.'),
                scanfolder = kwargs.get('scanfolder', './samples'),
                dbconnstr = kwargs.get('dbconnstr', 'mysql+pymysql://usrpcap:passwd@127.0.0.1/pcapdb'),
                # dbengine = None,
                lkp_geoip = kwargs.get('lkp_geoip','./res/dbip-country.csv'),
                lkp_country_names = kwargs.get('lkp_country_names', './res/country-names.csv'),
                lkp_iana_protocols = kwargs.get('lkp_iana_protocols', './res/protocol-numbers-1.csv'),
                localnet = kwargs.get('localnet','10.0.0.0/16'),
                
                chunksize = int(kwargs.get('chunksize', 10)),
                
                verbose = int(kwargs.get('verbose',1)),
                
                backup_folder = kwargs.get('backup_folder', './db_backup'),
                DB_MAX_TABLE_LEN = int(kwargs.get('DB_MAX_TABLE_LEN', 10 ** 6)),
                # run doctest and exit:
                doctest = int(kwargs.get('doctest', 0)),
                interval = int(kwargs.get('interval', 0))
            )

            # the full (?) path to script fiolder
            self.script_folder = os.path.split(sys.argv[0])[0]

            if self._buffer['ip6compat']:
                raise mdErrors.ECScannerError('IPv6 is not supported in the current version!')
            
        except ValueError as e:
            print("Error in arguments: invalid value")
            print(e)
            if TRACE_EXCEPTIONS: raise
            sys.exit(1)

        for name, value in self._buffer.items():
            setattr(self, name, value)
        
    def __str__(self):
        return "\n".join(\
            [\
                '{key}\t{val}'.format(key=key, val=val) \
                for key, val in self._buffer.items() \
                if key != 'dbconnstr'\
            ])

    # def connectdb(self):
    #     if not DB_ENABLED:
    #         raise mdErrors.EStorageError('DB operations are not supported in your environment.')
    #     if not self.dbengine:
    #         self.dbengine = _SQA.create_engine(self.dbconnstr)
    #     return self.dbengine


# ###########################################
# Settings()
# ###########################################

class Settings(object):
    def __init__(self, *args, **kwargs):
        self.vars = {}

    def updatefrom(self, source):
        if not isinstance(source, Settings):
            raise mdErrors.EEnvError('updatefrom() argument is not a Settings instance.')
        self.vars.update(source.vars)

class SettingsModule(Settings):
    """
    Uses values from module attributes.
    "kwargs" contains default values
    >>> e = SettingsModule('test-settings')
    >>> e.vars['module_test1']
    0
    >>> e.vars['module_test2']
    'text info'
    """
    def __init__(self, modname):

        Settings.__init__(self)

        if not modname:
            raise mdErrors.EEnvError('Specify the name of settings module!')

        try:
            settings = __import__(\
                modname,\
                globals={},\
                locals={},\
                fromlist=[],\
                level=-1)

        except ImportError as e:
            print(e)
            raise mdErrors.EEnvError('Cannot import settings from module')

        # import attributes extracted from module:

        for name in dir(settings):
            self.vars[name] = getattr(settings, name)


class CLI(Settings):
    """
    User settings from command-line arguments,
    overwrites default settings
    """

    def hint(self):
        #~ print ("""
        #~ Usage: {}
        #~ """.format(argv[0]))
        pass

    def __init__(self):

        Settings.__init__(self)

        try:
            for p in sys.argv[1:]:
                if p.find('=') >-1:
                    # named argument:
                    n, v = p.split('=')
                    self.vars[n] = v.replace('\"', '').replace('\'', '').strip(' ')
                    #~ print n, ':' , v.replace('\"', '').replace('\'', '')
                else:
                    raise mdErrors.EEnvError('Invalid argument pair without "=": {}'.format(p))
        except Exception as e:
            print ('Error in arguments: ', repr(e))
            self.hint()
            sys.exit(1)

