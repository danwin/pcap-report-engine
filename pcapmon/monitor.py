# substitute statement by build-in function
from __future__ import print_function

VERBOSE = True
TRACE_EXCEPTIONS = True

def warning(*args):
    if VERBOSE: print(*args)
