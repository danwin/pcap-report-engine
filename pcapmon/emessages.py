STEELSCRIPT_IMPORT_ERROR = \
"""
You need Riverbed 'steelscript' package installed.
Run 'pip install steelscript'.
After package installation, run:
<your python path>/Python-2.7.9/Scripts/steel install.
You can find installation details on Riverbeds' site
"""

STEELSCRIPT_WIRESHARK_IMPORT_ERROR = \
"""
You need Riverbed 'steelscript.wireshark' package installed.
Run 'pip install steelscript.wireshark'.
After package installation, run:
<your python path>/Python-2.7.9/Scripts/steel install.
You can find installation details on Riverbeds' site
"""

PANDAS_IMPORT_ERROR = \
"""
You need 'pandas' package installed.
"""

XLSWRITER_IMPPORT_ERROR = \
"""
Warning: You need xlsxwriter if you want to use Excel export.
Try to install it: "pip install xlsxwriter"
"""

DB_MOD_IMPORT_ERROR = \
"""
Warnings:

1. SQLAlchemy package necessary if you want to export reports in MySQL database.
To fix that, install SQLAlchemy package: "pip install sqlalchemy".

2. "pymysql" package necessary if you want to export reports in MySQL database.
To fix that, install "pymysql" package: "pip install pymysql".
"""

