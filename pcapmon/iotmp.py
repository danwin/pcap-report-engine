"""
Operations with a temporary storage
"""
import tempfile
import os

_tempfolder = tempfile.gettempdir()

def tmpstorage():
    return _tempfolder

def fullpath(fname):
    return os.path.join(_tempfolder, fname)


try:
    # test i/o access to tmp folder
    stamp = 'test string'
    tmpfilename = fullpath('testfile.txt')
    with open(tmpfilename, 'wb') as f:
        f.write(stamp)

    with open(tmpfilename, 'rb') as f:
        text = f.read()
        if text.find(stamp) < 0:
            raise Exception('Temporary storge error')
    #~ print 'TMP folder is ready: "{}"'.format(_tempfolder)
except IOError as e:
    print( \
        """TMP storage error:
        folder "{}" does not support i/o operations.
        Check access privilegies for that resource.
        Details: {!r}"""\
        .format(_tempfolder, e))
