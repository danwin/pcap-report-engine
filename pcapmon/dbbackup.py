#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# if "env" fails, change the first line to: #!/usr/bin/python
# substitute statement by build-in function
from __future__ import print_function

__VER__ = (0,9,10)


import sys

import os
import fnmatch
import datetime

from pcapmon import env

try:
    import pandas as pd
except ImportError:
    print('Critical error: "pandas" package not found.\nInstall "pandas": "pip install pandas".')
    sys.exit(1)

class ERptBackupError(Exception):
    """
    Generic error in module
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


class TableBackup(object):
    """
    """
    f_ext = 'dump'
    
    def __init__(self, rootpath=None, tablename=None):
        self.root = os.path.join(rootpath, tablename)
        #~ print(self.root)
        self.tablename = tablename
        self.files = []
        if not os.path.exists(self.root):
            os.makedirs(self.root)
        else:
            self._scanchunks()

    @staticmethod
    def _decode_fname(fname):
        name, counter, ext = fname.split('.')
        return name, counter, ext
    
    @staticmethod
    def df_size(dataframe):
        return dataframe.memory_usage(index=True).sum()
    
    def _scanchunks(self):
        # matched files: 'filename.counter.ext':
        f_pattern = '*.*.{}'.format(TableBackup.f_ext)
        # reset list:
        self.files = []
        for root, dirs, files in os.walk(self.root):
            for filename in fnmatch.filter(files, f_pattern):
                self.files.append(os.path.join(root, filename))
    
    @property
    def chunks_count(self):
        return len(self.files)
    
    def _create_filename(self):
        index = self.chunks_count + 1
        fname = '{}.{}.{}'.format(self.tablename, index, self.f_ext)
        return os.path.join(self.root, fname)
    
    def store(self, dataframe):
        try:
            fname = self._create_filename()
            dataframe.to_pickle(fname)
            self.files.append(fname)
        except IOError as e:
            print('Error writing backup for: {}'.format(tablename))
            print('Details: {!r}'.format(e))
            raise e
    
    def restore(self, engine):
        # truncate table
        from sqlalchemy.sql import text as _sqltext
        with engine.connect() as conn:
            try:
                trans = conn.begin()
                data = conn.execute(
                    _sqltext("""TRUNCATE TABLE {};""".format(self.tablename))
                    )
                trans.commit()
            except Exception as e:
                raise e 
        # restore
        for fname in self.files:
            try:
                df = pd.read_pickle(fname)
                df.to_sql(
                    self.tablename,
                    engine,
                    #~ schema=self.dbschema(),
                    # data type for each column:
                    #~ dtype = self.dbtypes,
                    # use index for DB column:
                    index=False,
                    # use index name from DataFrame:
                    index_label=None,
                    # append mode, create table if doesn't exist:
                    if_exists='append',
                    chunksize=1000)
            except Exception as e:
                raise EStorageError('Error on restoring report from backup "{}" to database: \n{!s}'.format(self.tablename, e))

class DatabaseBackup(object):
    """
    >>> dbb = DatabaseBackup('./test/testbackup')
    >>> import random
    >>> import numpy as np
    >>> df = pd.DataFrame(np.random.randn(100, 4), columns=list('ABCD'))
    >>> dbb.store(df, 'test_new_data')
    >>> dbb.active_partition
    
    >>> dbb.partitions
    
    >>> dbb.partition_path
    
    >>> dbb.table_backups
    
    >>> dbb.table_backups[0].files
    """

    def __init__(self, rootpath):
        self.rootpath = rootpath
        self.d_pattern = 'partition.*'
        self.active_partition = ''
        self.partitions = list()
        self.table_backups = dict()
        self._scanpartitions()
        self.select_last_partition()
        self.db_max_length = 0
    
    @property
    def partition_path(self):
        return os.path.join(self.rootpath, self.active_partition)

    def _scanpartitions(self):
        def cmp_sort(p1, p2):
            _, i1 = p1.split('.')
            _, i2 = p2.split('.')
            return int(i1) - int(i2)
        
        self.partitions = list()
        for root, dirs, files in os.walk(self.rootpath):
            for dirname in fnmatch.filter(dirs, self.d_pattern):
                self.partitions.append(dirname)
        self.partitions.sort(cmp_sort)
        
        if len(self.partitions) == 0:
            self.new_partition()
    
    def select_last_partition(self):
        if self.active_partition == self.partitions[-1]:
            return
        self.active_partition = self.partitions[-1]
        self._scantables()
    
    def select_partition(self, index):
        if len(self.partitions) < index < 0:
            raise ERptBackupError('Partition index out of range: {}'.format(index))
        self.active_partition = 'partition.{}'.format(index)
        self._scantables()
    
    def get_partition_info(self, index):
        if len(self.partitions) < index < 0:
            raise ERptBackupError('Partition index out of range: {}'.format(index))
        st_mtime = os.stat(os.path.join(self.rootpath, 'partition.{}'.format(index))).st_mtime
        return str(datetime.datetime.fromtimestamp(st_mtime))
        
    def new_partition(self):
        # close old partition:
        
        partition = 'partition.{}'.format(len(self.partitions))
        os.makedirs(os.path.join(self.rootpath, partition))
        self.partitions.append(partition)
        self.select_last_partition()

    def _scantables(self):
        # head, tail = os.path.split(path)
        self.table_backups = dict()
        for root, dirs, files in os.walk(self.partition_path):
            for folder in dirs:
                self.table_backups[folder] = TableBackup(rootpath=self.partition_path, tablename=folder)
                print('Backup table found: {}'.format(folder))

    def store(self, dataframe, tablename):
        self.select_last_partition()
        t_backup = self.table_backups.get(tablename, None)
        if t_backup is None:
            t_backup = TableBackup(rootpath=self.partition_path, tablename=tablename)
            self.table_backups[tablename] = t_backup
        t_backup.store(dataframe)

    def restore(self, tablename, engine):
        t_backup = self.table_backups.get(tablename, None)
        if t_backup is None:
            raise ERptBackupError
        t_backup.restore(engine)
    
    def restore_all(self, engine):
        for tablename, t_backup in self.table_backups.iteritems():
            t_backup.restore(engine)
            print('Table {} restored.'.format(tablename))

_instance = None

db_max_length = 0

def getinstance():
    global _instance
    _env = env.get()
    if not _instance:
        _instance = DatabaseBackup(_env.backup_folder)
    return _instance
