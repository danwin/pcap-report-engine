from pcapmon.core import \
    df_select_cols,\
    df_restrict_columns,\
    df_apply_source_info,\
    df_force_integer,\
    Report,\
    CounterReport,\
    MultirowCounterReport,\
    PCapView


# ###########################################
# DB field types (requires SQLAlchemy / pymysql)
# or works in a "stub" mode for file storages
# ###########################################

from pcapmon.dbi import \
    TEXT as _TEXT,\
    Integer as _Integer,\
    BigInteger as _BigInteger,\
    SmallInteger as _SmallInteger,\
    DateTime as _DateTime,\
    Float as _Float,\
    FDef

from pcapmon.monitor import warning, TRACE_EXCEPTIONS

import pandas
import datetime

# Shortcuts
_common_frame_fields = ['frame.number', 'frame.time']
_common_ip_fields = _common_frame_fields + ['ip.src', 'ip.dst', 'ip.ttl']

#~ _db_common_frame_fields = define_fields({'frame.number': 'BigInteger', 'frame.time': 'DateTime'})
#~ _db_common_ip_fields =  define_fields({\
    #~ 'ip.src': 'Text',\
    #~ 'ip.dst': 'Text',\
    #~ 'ip.ttl': 'SmallInteger'\
    #~ }, _db_common_frame_fields)

# ###########################################
# Views: DnsView()
# ###########################################
FLD_DNS_RESPONSE = 'dns.resp.addr'

class DnsView(PCapView):
    """
    DNS query and response
    """
    def qparams(self):
        #~ self.filterexpr = "udp.srcport==53"
        self.filterexpr = "dns.resp.len>0"
        self.fieldnames = _common_ip_fields + ['dns.qry.name', FLD_DNS_RESPONSE, 'dns.id', 'udp.srcport']

    def rmodel(self):
        return {
            'main': {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                # common ip fields:
                'frame.number'  :   FDef(_BigInteger), \
                'frame.time'    :   FDef(_DateTime), \

                'ip.src'    :   FDef(_TEXT), \
                'ip.dst'    :   FDef(_TEXT), \
                'ip.ttl'    :   FDef(_SmallInteger), \

                'dns.qry.name'  :   FDef(_TEXT), \
                FLD_DNS_RESPONSE : FDef(_TEXT), \
                #~ 'dns.srv.service'    :   FDef(_TEXT), \
                #~ 'dns.srv.port'   :   FDef(_Integer), \
                'dns.id'    :   FDef(_TEXT), \
                'udp.srcport'   :   FDef(_Integer) \
            },

            'dnscache' : {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'ip'    :   FDef(_TEXT),
                #~ 'dns.qry.name'  :   FDef(_TEXT),
                #~ 'dns.srv.service'   :   FDef(_TEXT),
                #~ 'dns.srv.port'  :   FDef(_TEXT),
                'hostname'  :   FDef(_TEXT),
            }
        }

    def processdata(self, data):
        df_apply_source_info(data, self)
        # handshake db table header and DataFrame header
        rview = df_select_cols(data, self.rfields['main'].keys())
        #~ rview = df_create_hash_view(rview)

        PCapView.processdata(self, rview)

        # create data frame and copy only 2 columns:
        dnscache = df_select_cols(data, [FLD_DNS_RESPONSE, 'dns.qry.name'])

        dnscache.rename(columns={FLD_DNS_RESPONSE:'ip', 'dns.qry.name':'hostname'}, inplace=True)

        dnscache = df_select_cols(dnscache, self.rfields['dnscache'].keys())

        df_apply_source_info(dnscache,self)

        rview = df_select_cols(dnscache, self.rfields['dnscache'].keys())
        #~ rview = df_create_hash_view(rview)

        self.addreport( \
                namesuffix='cache', \
                data=rview, \
                db_model = self.rfields['dnscache'] \
            )
        if self.lookups and self.lookups['dnscache']:
            cache = self.lookups['dnscache']
            i_response = self.fieldnames.index(FLD_DNS_RESPONSE)
            i_hostname = self.fieldnames.index('dns.qry.name')
            dnscount = 0
            for ndx, row in data.iterrows():
                if row[FLD_DNS_RESPONSE] and row['dns.qry.name']: 
                    cache.append(row[FLD_DNS_RESPONSE], row['dns.qry.name'])
                    dnscount += 1
                    #print(row[FLD_DNS_RESPONSE], row['dns.qry.name'], row.values[i_response], row[i_hostname])
                    #cache.append(row[i_response], row[i_hostname])
            warning('DNSCache: {} records added'.format(dnscount))
            #~ self.reports.append(Report(self,
                            #~ namesuffix='cache',
                            #~ data=pandas.DataFrame(data=cache.to_columns_dict()),
                            #~ dbtypes=define_fields({'ip': 'Text', 'hostname': 'Text'}),
                            #~ hashfields=['ip'],
                            #~ addtimerange=False
                            #~ ))


# ###########################################
# Views: TCPHandshakeView
# ###########################################

#~ use constant FIN => 0x01;
#~ use constant SYN => 0x02;
#~ use constant RST => 0x04;
#~ use constant PSH => 0x08;
#~ use constant ACK => 0x10;
#~ use constant URG => 0x20;
#~ use constant ECE => 0x40;
#~ use constant CWR => 0x80;

class TCPHandshakeView(PCapView):
    """
    TCP handshake.
    >>> ENV.outformat='console'
    >>> ENV.localnet = '192.168.0.0/16'
    >>> view = TCPHandshakeView()
    >>> from StringIO import StringIO
    >>> data = []
    >>> #data.append(','.join(view.fieldnames))
    >>> data.append('frame.number,  frame.time, ip.src, ip.dst, ip.ttl, tcp.stream, tcp.flags.syn,  tcp.flags.ack,  tcp.flags')
    >>> data.append('1, 00:00:45,   host23, host192,    52, 0,  0,  1,  16')
    >>> data.append('3, 00:00:59,   host192,    host216,    128,    2,  1,  0,  2')
    >>> data.append('7, 00:00:00,   host216,    host192,    50, 2,  1,  1,  18')
    >>> data.append('8, 00:00:00,   host192,    host216,    128,    2,  0,  1,  16')
    >>> data.append('4, 00:00:59,   host192,    host216,    128,    3,  1,  0,  2')
    >>> data.append('13,    00:00:00,   host216,    host192,    49, 3,  1,  1,  18')
    >>> data.append('14,    00:00:00,   host192,    host216,    128,    3,  0,  1,  16')
    >>> data.append('5, 00:00:00,   host192,    host54, 128,    4,  1,  0,  2')
    >>> data.append('17,    00:00:01,   host54, host192,    42, 4,  1,  1,  18')
    >>> data.append('18,    00:00:01,   host192,    host54, 128,    4,  0,  1,  16')
    >>> data.append('6, 00:00:00,   host192,    host104,    128,    5,  1,  0,  2')
    >>> data.append('21,    00:00:01,   host104,    host192,    53, 5,  1,  1,  18')
    >>> data.append('22,    00:00:01,   host192,    host104,    128,    5,  0,  1,  16')
    >>> data.append('9, 00:00:00,   host192,    host54, 128,    6,  1,  0,  2')
    >>> data.append('27,    00:00:02,   host54, host192,    43, 6,  1,  1,  18')
    >>> data.append('28,    00:00:02,   host192,    host54, 128,    6,  0,  1,  16')
    >>> data.append('29,    00:00:22,   hack-SYN,   host192,    128,    16, 1,  0,  0x02')
    >>> data.append('30,    00:00:42,   hack-SYN,   host192,    128,    26, 1,  0,  0x02')
    >>> data.append('32,    00:00:62,   single-ACK, host192,    128,    46, 0,  1,  0x10')
    >>> #df = pandas.read_csv(StringIO('\\r\\n'.join(data)), names=view.fieldnames, header=None)
    >>> df = pandas.read_csv(StringIO('\\r\\n'.join(data)))
    >>> view.processdata(df, None, None)
    >>> view.save()
        frame.number                   frame.time  tcp.stream    flags syn.ttl  \\
    1              3          00:00:59           2      SYN     128
    2              7          00:00:00           2  SYN-ACK
    3              8          00:00:00           2      ACK
    4              4          00:00:59           3      SYN     128
    5             13          00:00:00           3  SYN-ACK
    6             14          00:00:00           3      ACK
    7              5          00:00:00           4      SYN     128
    8             17          00:00:01           4  SYN-ACK
    9             18          00:00:01           4      ACK
    10             6          00:00:00           5      SYN     128
    11            21          00:00:01           5  SYN-ACK
    12            22          00:00:01           5      ACK
    13             9          00:00:00           6      SYN     128
    14            27          00:00:02           6  SYN-ACK
    15            28          00:00:02           6      ACK
    <BLANKLINE>
       synack.ttl ack.ttl              client              server
    1                          host192        host216
    2          50              host192        host216
    3                 128      host192        host216
    4                          host192        host216
    5          49              host192        host216
    6                 128      host192        host216
    7                          host192      host54
    8          42              host192      host54
    9                 128      host192      host54
    10                         host192        host104
    11         53              host192        host104
    12                128      host192        host104
    13                         host192      host54
    14         43              host192      host54
    15                128      host192      host54
    """
    def qparams(self):
        #~ filterexpr = "tcp.flags.syn==1 or (tcp.seq==1 and tcp.ack==1 and tcp.len==0 and tcp.analysis.initial_rtt)"
        #~ self.filterexpr = "tcp.flags.syn==1 || (tcp.seq==1 && tcp.ack==1 && tcp.len==0) || tcp.flags==1 || (tcp.flags.syn && tcp.flags.fin)"
        self.filterexpr = "tcp.flags.syn==1 || (tcp.ack==1 && tcp.len==0)"
        self.fieldnames = _common_ip_fields + ['tcp.stream', 'tcp.flags.syn', 'tcp.flags.ack', 'tcp.flags.fin', 'tcp.flags']

    def rmodel(self):
        return {
            'main':{
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'tcp.stream'    :   FDef(_BigInteger),
                'frame.number'  :   FDef(_BigInteger),
                'flags' :   FDef(_TEXT),

                'ip.ttl'    :   FDef(_SmallInteger),

                'syn.ttl'   :   FDef(_TEXT),
                'synack.ttl'    :   FDef(_TEXT),
                'ack.ttl'   :   FDef(_TEXT),
                'tcp.flags' :   FDef(_Integer),
                #~ 'syn.ttl'    :   FDef(_SmallInteger),
                #~ 'synack.ttl' :   FDef(_SmallInteger),
                #~ 'ack.ttl'    :   FDef(_SmallInteger),
                'client'    :   FDef(_TEXT),
                'server'    :   FDef(_TEXT),
                'frame.time'    :   FDef(_DateTime),
            }
        }

    def processdata(self, data):

        def hscode(row):
            tcp_flags = df_force_integer(row['tcp.flags'])
            tcp_syn = df_force_integer(row['tcp.flags.syn'])
            tcp_ack = df_force_integer(row['tcp.flags.ack'])
            tcp_fin = df_force_integer(row['tcp.flags.fin'])
            if tcp_flags > 0:
                if tcp_syn:
                    if tcp_fin:
                        return -2 # '!syn+fin'
                    if tcp_ack:
                        return 2 # 'SYN-ACK'
                    return 1 # 'SYN'
                if tcp_fin:
                    return 4 # 'FIN'
                if tcp_ack:
                    return 3 # 'ACK'
                return -1 # unknown
            return 0 # zero flags

        def legend(row):
            return {
                -2: '*syn+fin',
                -1: ' ',
                0: '*zero',
                1: 'SYN',
                2: 'SYN-ACK',
                3: 'ACK',
                4: 'FIN'
            }.get(hscode(row), '')


        data['tcp.flags'] = data['tcp.flags'].apply(df_force_integer)
        data['tcp.flags.syn'] = data['tcp.flags.syn'].apply(df_force_integer)
        data['tcp.flags.ack'] = data['tcp.flags.ack'].apply(df_force_integer)

        data['flags'] = data.apply(legend, axis=1)
        data['hscode'] = data.apply(hscode, axis=1)

        #~ data = data[data['flags'] > 0]

        data['syn.ttl'] = data.apply(lambda r: r['ip.ttl'] if r['hscode']==1 else '', axis=1)
        data['synack.ttl'] = data.apply(lambda r: r['ip.ttl'] if r['hscode']==2 else '', axis=1)
        data['ack.ttl'] = data.apply(lambda r: r['ip.ttl'] if r['hscode']==3 else '', axis=1)

        data['client'] = data.apply(lambda r: r['ip.src'] if r['hscode'] in (1, 3, 4) else r['ip.dst'], axis=1)
        data['server'] = data.apply(lambda r: r['ip.src'] if r['hscode'] not in (1, 3, 4) else r['ip.dst'], axis=1)

        #~ data = data.groupby(['tcp.stream']).filter(lambda x: x['hscode'] in (1,2,3)).fillna(0).max()

        #~ grouped['syn.ttl'].apply(lambda x: x.max())
        #~ grouped['synack.ttl'].apply(lambda x: x.max())
        #~ grouped['ack.ttl'].apply(lambda x: x.max())

        #~ data.groupby(['server','client']).agg({
            #~ 'syn.ttl': lambda x: x.max(),
            #~ 'synack.ttl': lambda x: x.max(),
            #~ 'ack.ttl': lambda x: x.max(),
        #~ }).reset_index()

        #~ data.sort(['tcp.stream'])

        #~ del data['frame.number']

        #~ grouped = data.groupby(['tcp.stream', 'ip.src','ip.dst','syn.ttl', 'synack.ttl', 'ack.ttl'])
        #~ data.sort(['tcp.stream', 'frame.number'])


        data = data.groupby(['tcp.stream']).filter(lambda x: len(x) > 1)

        # do not try to process an empty dataset after filtering:
        if len(data.index) == 0:
            warning('Empty dataset after filtering...')
            return

        data = data.sort(['tcp.stream', 'flags', 'frame.number'])

        data.reset_index(inplace=True)

        df_apply_source_info(data, self)
        # handshake db table header and DataFrame header
        data = df_select_cols(data, self.rfields['main'].keys())
        #~ data = df_create_hash_view(data)

        #~ data.reset_index()

        # ======

        #~ rep2 = data.pivot(index='tcp.stream', columns='flags', values='ip.ttl')

        #~ summary = Report(namesuffix='agg', data=rep2)
        #~ self.reports.append(summary)

        # ======

        #~ data = data.groupby(['server','client','syn.ttl','synack.ttl','ack.ttl']).agg({
            #~ 'syn.ttl': lambda x: x.max(),
            #~ 'synack.ttl': lambda x: x.max(),
            #~ 'ack.ttl': lambda x: x.max(),
        #~ }).reset_index()


        #~ data.groupby('tcp.stream')['syn.ttl'].apply(lambda x: x.max())
        #~ data.groupby('tcp.stream')['synack.ttl'].apply(lambda x: x.max())
        #~ data.groupby('tcp.stream')['ack.ttl'].apply(lambda x: x.max())

        #~ index = [gp_keys[0] for gp_keys in grouped.groups.values()]
        #~ data = data.reindex(index)

        #~ data = grouped
        #~ .filter(lambda x: len(x) > 1)

        #~ grouped = data.groupby(['tcp.stream']).agg({
            #~ 'syn.ttl': lambda x: x.max(),
            #~ 'synack.ttl': lambda x: x.max(),
            #~ 'ack.ttl': lambda x: x.max(),
        #~ }).reset_index()

        #~ data =grouped.max().reset_index()
        #~ data = grouped

        #~ data.sort(['frame.number'])

        #~ data = data.groupby(['tcp.stream'])

        #~ data = data.groupby(['tcp.stream','syn.ttl']).max().reset_index()
        #~ data = data.groupby(['tcp.stream','synack.ttl']).max().reset_index()
        #~ data = data.groupby(['tcp.stream','ack.ttl']).max().reset_index()

        #~ data = data.groupby(['syn.ttl']).apply(lambda x: x.max())
        #~ data = data.groupby(['synack.ttl']).apply(lambda x: x.max())
        #~ data = data.groupby(['ack.ttl']).apply(lambda x: x.max())


        #~ index = [gp_keys[0] for gp_keys in grouped.groups.values()]
        #~ data = data.reindex(index)

        #~ data_complete = data.groupby('tcp.stream')
        # .filter(lambda x: len(x) > 2)

        #~ data_noack = data.groupby('tcp.stream').filter(lambda x: len(x) < 3)
        #~ data_noack.filter(lambda x: x['ack'] == 0)

        #~ del data_complete['tcp.flags']
        #~ del data_noack['tcp.flags']

        PCapView.processdata(self, data)
        #~ self.reports.append(Report(namesuffix='full', data = data_complete))
        #~ self.reports.append(Report(namesuffix='noack', data = data_noack))


# ###########################################
# Views: PingView
# ###########################################


"""
'frame.number', 'frame.time', 'ip.src', 'ip.dst', 'ip.ttl'
"""

class PingView(PCapView):
    """
    Incoming PING requests.
    Output is in 2 reports:
    1. All ping packets (wth 'frame.time' and 'frame.number')
    2. Aggregated data - total attempts, grouped by remote host and destination.
    >>> ENV.outformat='console'
    >>> ENV.localnet = '192.168.0.0/16'
    >>> view = PingView()
    >>> from StringIO import StringIO
    >>> data = [','.join(view.fieldnames)]
    >>> data.append('1,2015-10-01 00:00:00,200.200.200.1,192.168.0.10,128')
    >>> data.append('2,2015-10-01 00:00:01,200.200.200.1,192.168.0.10,128')
    >>> data.append('3,2015-10-01 00:00:02,200.200.200.1,192.168.0.15,128')
    >>> data.append('4,2015-10-01 00:00:03,200.200.200.2,192.168.0.10,128')
    >>> data.append('5,2015-10-01 00:00:04,200.200.200.2,192.168.0.10,128')
    >>> df = pandas.read_csv(StringIO('\\r\\n'.join(data)))
    >>> view.processdata(df, None, None)
    >>> view.save()
       frame.number           frame.time         ip.src        ip.dst  ip.ttl
    0             1  2015-10-01 00:00:00  200.200.200.1  192.168.0.10     128
    1             2  2015-10-01 00:00:01  200.200.200.1  192.168.0.10     128
    2             3  2015-10-01 00:00:02  200.200.200.1  192.168.0.15     128
    3             4  2015-10-01 00:00:03  200.200.200.2  192.168.0.10     128
    4             5  2015-10-01 00:00:04  200.200.200.2  192.168.0.10     128
              ip.src        ip.dst  total.attempts
    0  200.200.200.1  192.168.0.10               2
    1  200.200.200.1  192.168.0.15               1
    2  200.200.200.2  192.168.0.10               2
    """
    def qparams(self):
        self.filterexpr = "icmp.type==8 && ip.dst=={localnet}" # "8" is a type of incoming echo
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = _common_ip_fields

    def rmodel(self):
        return {

            'main' : {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                # common ip fields:
                'frame.number'  :   FDef(_BigInteger), \
                'frame.time'    :   FDef(_DateTime), \

                'ip.src'    :   FDef(_TEXT), \
                'ip.dst'    :   FDef(_TEXT), \
                'ip.ttl'    :   FDef(_SmallInteger) \
            },

            'aggregation' : {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'ip.src'    :   FDef(_TEXT),
                'ip.dst'    :   FDef(_TEXT),
                'total.attempts'    :   FDef(_Integer)
            }
        }

    def processdata(self, data):
        df_apply_source_info(data, self)
        data = df_select_cols(data, self.rfields['main'].keys())
        #~ data = df_create_hash_view(data)

        data.sort(['ip.src', 'ip.dst'])

        PCapView.processdata(self, data)

        # aggregation report:

        grouped = data.groupby(['ip.src', 'ip.dst', 'frame.number'])

        # aggregation by attempts count:
        # grouped= grouped.count('frame.number').reset_index()
        grouped= grouped.count().reset_index()

        # rename col:
        grouped.rename(columns={'frame.number':'total.attempts'}, inplace=True)

        df_apply_source_info(grouped, self)
        grouped = df_select_cols(grouped, self.rfields['aggregation'].keys())
        #~ grouped = df_create_hash_view(grouped)

        self.addreport(\
                namesuffix='aggregation',\
                data=grouped,
                db_model = self.rfields['aggregation']
            )

# ###########################################
# Views: TrafficToCountriesView
# ###########################################

class TrafficToCountriesView(PCapView):
    def qparams(self):
        self.filterexpr = '(ip.src!={localnet} || ip.dst!={localnet})'
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['frame.number','frame.time','ip.proto', 'frame.len', 'ip.src', 'ip.dst', 'tcp.dstport', 'udp.dstport','tcp.srcport', 'udp.srcport']

    def rmodel(self):
        return {
            'main' : {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'frame.number'  :   FDef(_BigInteger),
                'frame.time'    :   FDef(_DateTime), \
                'ip.proto'  :   FDef(_SmallInteger),
                'frame.len' :   FDef(_Integer),

                #~ 'origin.src' : FDef(_TEXT),
                #~ 'origin.dst' : FDef(_TEXT),
                'direction' : FDef(_TEXT),
                
                'local.ip' : FDef(_TEXT),
                'remote.ip' : FDef(_TEXT),
                'remote.hostname' : FDef(_TEXT),
                'country.abbr' : FDef(_TEXT),
                'country.name' : FDef(_TEXT),
                
                'ip.src'    :   FDef(_TEXT),
                'ip.dst'    :   FDef(_TEXT),
                
                'service.port' : FDef(_Integer), # computed
                
                #~ 'tcp.dstport'   :   FDef(_Integer),
                #~ 'udp.dstport'   :   FDef(_Integer),
                #~ 'tcp.srcport'   :   FDef(_Integer),
                #~ 'udp.srcport'   :   FDef(_Integer),
                
                #~ 'country.abbr'  :   FDef(_TEXT),
                #~ 'country.name'  :   FDef(_TEXT),
                #~ 'hostname'  :   FDef(_TEXT),
                
                'proto.iana'    :   FDef(_TEXT)
            }
        }

    def processdata(self, data):
        def service_port(r):
            suffix = '.srcport' if r['direction'] == 'in' else '.dstport'
            proto = (r['proto.iana'] or '').lower()
            if proto == 'tcp':
                return r['tcp'+suffix]
            if proto == 'udp':
                return r['udp'+suffix]
            # icmp has no ports so return nothing!
    
        df_apply_source_info(data, self)
        #~ data = df_create_hash_view(data)
        
                #~ 'hostname.src'    :   FDef(_TEXT),
                #~ 'hostname.dst'    :   FDef(_TEXT),

        data['origin.src'] = data['ip.src'].map(self.lookups['iplookup'].get)
        data['origin.dst'] = data['ip.dst'].map(self.lookups['iplookup'].get)
        data['direction'] = data.apply(lambda r: 'out' if r['origin.src'] == 'local' else 'in', axis=1)
        
        
        data['local.ip'] = data.apply(lambda r: r['ip.src'] if r['direction'] == 'out' else r['ip.dst'], axis=1)
        data['remote.ip'] = data.apply(lambda r: r['ip.dst'] if r['direction'] == 'out' else r['ip.src'], axis=1)
        data['remote.hostname'] = data['remote.ip'].map(self.lookups['dnscache'].get)
        #~ data['country.abbr'] = data['remote.ip'].map(self.lookups['iplookup'].get)
        data['country.abbr'] = data.apply(lambda r: r['origin.dst'] if r['origin.src'] == 'local' else r['origin.src'], axis=1)
        data['country.name'] = data['country.abbr'].map(self.lookups['country_names'].get)

        #~ data['country.abbr'] = data['ip.dst'].map(self.lookups['iplookup'].get)
        #~ data['country.name'] = data['country.abbr'].map(self.lookups['country_names'].get)
        data['proto.iana'] = data['ip.proto'].map(self.lookups['protolookup'].get)
        
        data['service.port'] = data.apply(service_port, axis=1)
        #~ data['hostname'] = data['ip.dst'].map(self.lookups['dnscache'].get)

        data = df_select_cols(data, self.rfields['main'].keys())
        
        # replace NA with ''
        #~ data = data.fillna('', )
        
        PCapView.processdata(self, data)

class TrafficToCountriesViewExtended(TrafficToCountriesView):
    """
    View with 2 sub-reports.
    """
    def rmodel(self):
        model = TrafficToCountriesView.rmodel(self)
        model['aggregation'] = {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'bytes.total' :   FDef(_BigInteger),

                #~ 'origin.src' : FDef(_TEXT),
                #~ 'origin.dst' : FDef(_TEXT),
                'direction' : FDef(_TEXT),
                
                #~ 'local.ip' : FDef(_TEXT),

                'remote.ip' : FDef(_TEXT),
                'remote.hostname' : FDef(_TEXT),
                'country.abbr' : FDef(_TEXT),
                'country.name' : FDef(_TEXT),
                
                #~ 'tcp.dstport'   :   FDef(_Integer),
                #~ 'udp.dstport'   :   FDef(_Integer),
                
                'proto.iana'    :   FDef(_TEXT),
                'service.port' : FDef(_Integer), # computed
        }
        return model
    
    def processdata(self, data):
        TrafficToCountriesView.processdata(self, data)
        #extract changed data from report (data was changed by inherited processdata):
        data = self.reports[0].df
        # create aggregation
        #~ print(df.columns.names)
        #~ summary = data.set_index(['country.abbr', 'country.name', 'remote.hostname', 'remote.ip', 'proto.iana', 'direction'])
        summary = data.groupby(['country.abbr', 'country.name', 'remote.hostname', 'remote.ip', 'proto.iana', 'service.port', 'direction'], as_index=False).sum()

        summary.rename(columns={'frame.len':'bytes.total'}, inplace=True)

        df_apply_source_info(summary, self)
        summary = df_select_cols(summary, self.rfields['aggregation'].keys())

        self.addreport(
                namesuffix='aggregation', 
                data=summary,
                db_model = self.rfields['aggregation']
            )

# ###########################################
# Views: TorrentView
# ###########################################

_base = TrafficToCountriesView

class TorrentView(_base):
    
    def qparams(self):
        _base.qparams(self)
        self.params = None
        self.filterexpr = '(bittorrent) or (http.request and (http contains "scrape" or http contains "announce"))'
        #~ self.filterexpr = '(bittorrent and bittorrent.info_hash) or (http.request and (http contains "scrape" or http contains "announce"))'
        self.fieldnames = self.fieldnames + [
            'bittorrent.peer_id',
            'bittorrent.port',
            'bittorrent.protocol.name',
            'bittorrent.info_hash',
            #~ 'bittorrent.jpc.session',
            #~ 'bittorrent.jpc.addr',
            #~ 'bittorrent.jpc.port',
            'bittorrent.msg',
            'bittorrent.msg.length',
            'bittorrent.piece.length',            
            
            'http.request.full_uri'
        ]

    def rmodel(self):
        model = _base.rmodel(self)
        model['main'].update({
            'bittorrent.peer_id': FDef(_TEXT),
            'torrent.client': FDef(_TEXT), # computed
            'bittorrent.port': FDef(_Integer),
            'bittorrent.protocol.name': FDef(_TEXT),
            'bittorrent.info_hash': FDef(_TEXT),
            #~ 'bittorrent.jpc.session': FDef(_BigInteger),
            #~ 'bittorrent.jpc.addr': FDef(_TEXT),
            #~ 'bittorrent.jpc.port': FDef(_BigInteger),
            'bittorrent.msg': FDef(_TEXT),
            #~ 'bittorrent.msg.length': FDef(_BigInteger),
            #~ 'bittorrent.piece.length': FDef(_BigInteger),
            'bittorrent.msg.length': FDef(_BigInteger),
            'bittorrent.piece.length': FDef(_BigInteger),
            
            'http.request.full_uri': FDef(_TEXT),
            
        })
        #~ del model['main']['direction']
        return model
    
    def processdata(self, data):
        data['torrent.client'] = data['bittorrent.peer_id'].map(lambda v: ''.join([chr(int(o, 16)) for o in v.split(':')[:8]]) if v and len(v)>30 else '')
        data['bittorrent.piece.length'] = data['bittorrent.piece.length'].apply(df_force_integer)
        data['bittorrent.msg.length'] = data['bittorrent.msg.length'].apply(df_force_integer)
        _base.processdata(self, data)

# ###########################################
# Views: UDPView
# ###########################################

class UDPView(PCapView):
    def qparams(self):
        self.filterexpr = 'udp && (ip.dst!={localnet} || ip.src!={localnet})'
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['frame.time', 'frame.number', 'ip.src', 'udp.dstport']

    def rmodel(self):
        return {
            'main' : {
                # Service fields:
                'pcap'  :   FDef(_TEXT), \
                'pcap_first_timestamp'  :   FDef(_BigInteger), \
                'pcap_last_timestamp'   :   FDef(_BigInteger), \

                'frame.number'  :   FDef(_BigInteger),
                'frame.time'    :   FDef(_DateTime),
                'ip.src'    :   FDef(_TEXT),
                'udp.dstport'   :   FDef(_Integer)
            }
        }

    def processdata(self, data):

        df_apply_source_info(data, self)
        data = df_select_cols(data, self.rfields['main'].keys())
        #~ data = df_create_hash_view(data)

        PCapView.processdata(self, data)

class CounterView(PCapView):
    
    def __init__(self, rptfname=None, lookups=None, processing_time=None):
        PCapView.__init__(self, rptfname, lookups)
        self.processing_time = processing_time

    def qparams(self):
        self.filterexpr = 'http || ftp || tcp || udp || bittorrent || icmp'
        #self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['http', 'ftp', 'tcp', 'udp', 'bittorrent', 'icmp']

    def rmodel(self):
        return {
            'main' : {
                # Service fields:
                'processing_time' :	FDef(_Float),
                'files_count' : FDef(_BigInteger),
                
                'http'  :   FDef(_BigInteger), \
                'ftp'  :   FDef(_BigInteger), \
                'tcp'  :   FDef(_BigInteger), \
                'udp'  :   FDef(_BigInteger), \
                'torrent'  :   FDef(_BigInteger), \
                'icmp'  :   FDef(_BigInteger), \

                'process_status' : FDef(_Integer)

            }
        }
    
    def addreport(self, data=None, **kwargs):
        _report = CounterReport(self, data=data, **kwargs)
        self.reports.append(_report)
    
    def processdata(self, data):
        if data is None:
            return
        data = data.fillna(value=0)
        cols = self.rfields['main'].keys()
        buff = pandas.DataFrame(index=[0], columns=cols, dtype='int')
        for col in self.fieldnames:
            buff[col] =  len(data[data[col]>0])
        buff['processing_time'] = self.processing_time
        buff['files_count'] = 1
        buff.rename(columns={'bittorrent': 'torrent'}, inplace=True)

        buff['process_status'] = 0

        buff = df_select_cols(buff, cols)
        PCapView.processdata(self, buff)				

# ###########################################
# Views: ViewInboundTrafficByPeers
# ###########################################

class ViewInboundTrafficByPeers(CounterView):

    def qparams(self):
        self.filterexpr = '(ip.src!={localnet}) && (ip.dst=={localnet})'
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['frame.number', 'ip.src', 'ip.dst']


    def rmodel(self):
        return {
            'main' : {
                # Service fields:

                'ip.src' : FDef(_TEXT),
                'ip.dst' : FDef(_TEXT),
                'country.name' : FDef(_TEXT),
                'frames.total' :FDef(_BigInteger),

                'date_added' : FDef(_DateTime),
                'is_bad_record' : FDef(_SmallInteger),
                'is_acknowledged' : FDef(_SmallInteger),
                'latitude': FDef(_Float),
                'longitude': FDef(_Float) 
            }
    }


    def addreport(self, data=None, **kwargs):
        _report = MultirowCounterReport(self, data=data, group_cols=['ip.src', 'ip.dst', 'country.name'], **kwargs)
        self.reports.append(_report)

    def processdata(self, data):
        data = data.groupby(['ip.src', 'ip.dst']).count().reset_index()

        # decode source:
        data['country.abbr'] = data['ip.src'].map(self.lookups['iplookup'].get)

        # data['country.abbr'] = data.apply(lambda r: r['origin.src'], axis=1)
        data['country.name'] = data['country.abbr'].map(self.lookups['country_names'].get)

        # print(data.head(50))
 
        data.rename(columns={'frame.number': 'frames.total'}, inplace=True)

        # add new fields

        data['date_added'] = datetime.datetime.now()
        data['is_bad_record'] = 0
        data['is_acknowledged'] = 0
        data['latitude'] = None
        data['longitude'] = None

        data = df_select_cols(data, self.rfields['main'].keys())
        PCapView.processdata(self, data)
        
# ###########################################
# Views: ViewOutboundTrafficByPeers
# ###########################################

class ViewOutboundTrafficByPeers(ViewInboundTrafficByPeers):

    def qparams(self):
        ViewInboundTrafficByPeers.qparams(self)
        self.filterexpr = '(ip.src=={localnet}) && (ip.dst!={localnet})'

    def processdata(self, data):
        data = data.groupby(['ip.src', 'ip.dst']).count().reset_index()

        # decode destination:
        data['country.abbr'] = data['ip.dst'].map(self.lookups['iplookup'].get)

        # data['country.abbr'] = data.apply(lambda r: r['origin.src'], axis=1)
        data['country.name'] = data['country.abbr'].map(self.lookups['country_names'].get)

        # print(data.head(50))
 
        data.rename(columns={'frame.number': 'frames.total'}, inplace=True)
       
        data = df_select_cols(data, self.rfields['main'].keys())
        PCapView.processdata(self, data)

# qry_tcp_handshake = flt_tcp_handshake + " -T fields -e tcp.stream | sort -n | uniq"


# ###########################################
# Views: DnsCountByPeers
# ###########################################

class DnsCountByPeers(CounterView):

    def qparams(self):

        self.filterexpr = '(ip.src=={localnet}) && (ip.dst!={localnet}) && dns.qry.name'
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['frame.number', 'ip.src', 'ip.dst']

    def rmodel(self):
        return {
            'main' : {
                # Service fields:

                'ip.src' : FDef(_TEXT),
                'ip.dst' : FDef(_TEXT),
                'frames.total' :FDef(_BigInteger),
                
                'date_added' : FDef(_DateTime),
                'is_bad_dns' : FDef(_SmallInteger),
                'is_acknowledged' : FDef(_SmallInteger)
            }
    }

    def addreport(self, data=None, **kwargs):
        _report = MultirowCounterReport(self, data=data, group_cols=['ip.src', 'ip.dst'], **kwargs)
        self.reports.append(_report)

    def processdata(self, data):
        data = data.groupby(['ip.src', 'ip.dst']).count().reset_index()

        # print(data.head(50))
 
        data.rename(columns={'frame.number': 'frames.total'}, inplace=True)

        # add new columns
        data['date_added'] = datetime.datetime.now()
        data['is_bad_dns'] = 0
        data['is_acknowledged'] = 0

        data = df_select_cols(data, self.rfields['main'].keys())
        PCapView.processdata(self, data)


# ###########################################
# Views: ProtocolsCountByPeers
# ###########################################

class ProtocolsCountByPeers(CounterView):

    def qparams(self):
        self.filterexpr = '(ip.src=={localnet}) && (ip.dst!={localnet}) && (http || ftp || tcp || udp || bittorrent || icmp)'
        self.params = {'localnet':'192.168.0.0/16'} # param name : default value
        self.fieldnames = ['frame.number', 'ip.src', 'ip.dst', 'http', 'ftp', 'tcp', 'udp', 'bittorrent', 'icmp']


    def rmodel(self):
        return {
            'main' : {
                # Service fields:

                'ip.src' : FDef(_TEXT),
                'ip.dst' : FDef(_TEXT),
                'protocol.name' : FDef(_TEXT),
                'frames.total' :FDef(_BigInteger),

                'date_added' : FDef(_DateTime),
                'is_bad_record' : FDef(_SmallInteger),
                'is_acknowledged' : FDef(_SmallInteger)
            }
    }

    def addreport(self, data=None, **kwargs):
        _report = MultirowCounterReport(self, data=data, group_cols=['ip.src', 'ip.dst', 'protocol.name'], **kwargs)
        self.reports.append(_report)

    def processdata(self, data):

        # detect protocols
        def decoder(r):
            if r['http']:
                return 'HTTP'
            if r['ftp']:
                return 'FTP'
            if r['icmp']:
                return 'ICMP'
            if r['bittorrent']:
                return 'Torrent'
            if r['udp']:
                return 'UDP'
            if r['tcp']:
                return 'TCP'

        data['protocol.name'] = data.apply(decoder, axis=1)
        del data['http']
        del data['ftp']
        del data['icmp']
        del data['bittorrent']
        del data['udp']
        del data['tcp']

        # print(data.head(50))

        data = data.groupby(['ip.src', 'ip.dst', 'protocol.name']).count().reset_index()
 
        data.rename(columns={'frame.number': 'frames.total'}, inplace=True)       

        # add new columns
        data['date_added'] = datetime.datetime.now()
        data['is_bad_dns'] = 0
        data['is_acknowledged'] = 0

        data = df_select_cols(data, self.rfields['main'].keys())
        PCapView.processdata(self, data)

