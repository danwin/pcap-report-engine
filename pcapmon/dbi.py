from pcapmon import \
    emessages as _msg,\
    errors as mdErrors,\
    env as mdEnv,\
    monitor

warning = monitor.warning
_getEnv = mdEnv.get

try:
    DB_ENABLED = True
    import pymysql
    import sqlalchemy as _SQA
    from sqlalchemy import \
        MetaData as _MetaData,\
        TEXT,\
        Integer,\
        BigInteger,\
        SmallInteger,\
        DateTime,\
        Float

    from sqlalchemy.sql import \
        text as _sqltext

except ImportError:

    DB_ENABLED = False
    warning (_msg.DB_MOD_IMPORT_ERROR)


# ###########################################
# Data / DB tools:
# ###########################################

class FDef(object):
    """ Record for SQLAlchemy-compatible definition of column/field """
    def __init__(self, ftype, *args, **kwargs):
        self.ftype = ftype
        self.args = args
        self.kwargs = kwargs

def tofieldname(name):
    """
    Translate name to "safe" MySQL table name or field name without special chars.
    """
    return name.replace('.', '_').replace(' ', '_')

def is_pcap_in_db(engine, tablename, fpcapname, ms_start, ms_end):
    if not DB_ENABLED:
        return False
    with engine.connect() as conn:
        try:
            data = conn.execute(
                _sqltext("""SELECT COUNT(rec_id)
                    FROM {} WHERE (
                        pcap_first_timestamp = :ms_start AND
                        pcap_last_timestamp = :ms_end AND
                        pcap = :fpcapname
                    );""".format(tablename)),
                dict(
                    fpcapname=fpcapname,
                    ms_start=ms_start,
                    ms_end=ms_end
                )
            ).fetchone()
            count = data[0]
            return count > 0
        except:
            return False

def count_table_length(engine, tablename):
    if not DB_ENABLED:
        return 0
    with engine.connect() as conn:
        try:
            data = conn.execute(
                _sqltext("""SELECT COUNT(rec_id)
                    FROM {};""".format(tablename))
            ).fetchone()
            count = data[0]
            return count
        except:
            return False

def truncate_table(engine, tablename):
    if not DB_ENABLED:
        return True
    with engine.connect() as conn:
        try:
            trans = conn.begin()
            data = conn.execute(
                _sqltext("""TRUNCATE TABLE {};""".format(tablename))
                )
            trans.commit()
        except Exception as e:
            if TRACE_EXCEPTIONS: raise e 
            return False


# ###########################################
# DB-only tools (requires SQLAlchemy / pymysql)
# ###########################################

if DB_ENABLED:
    # shortcuts for SQLAlchemy class~es:
    _MetaData = _SQA.MetaData
    TEXT = _SQA.TEXT
    Integer = _SQA.Integer
    BigInteger = _SQA.BigInteger
    SmallInteger = _SQA.SmallInteger
    DateTime = _SQA.DateTime
    Float = _SQA.Float

    def Field(name, *args, **kwargs):
        return _SQA.Column(tofieldname(name), *args, **kwargs)

    def TableDef(engine, tablename, *args, **kwargs):
        """
        Define and create SQLAlchemy table (if not exists)
        """
        kwargs = kwargs or {}
        meta = _MetaData(bind=engine)
        _checkfirst = not kwargs.pop('rewrite', True)
        table = _SQA.Table(
            tablename,
            meta,
            # add autoincrement key (specific to DB stotage only):
            Field('rec_id', BigInteger, primary_key=True, autoincrement=True),
            *args,
            **kwargs)
        if not _checkfirst:
            table.drop(engine, checkfirst=True)
        table.create(engine, checkfirst=_checkfirst)
        return table

    def TableFromModel(engine, tablename, model, **kwargs):
        return \
            TableDef(engine, tablename,
                *[Field(name, fdef.ftype, *fdef.args, **fdef.kwargs) for name, fdef in model.iteritems()],
                **kwargs)
else:
    _SQA = \
    _MetaData = \
    TEXT = \
    Integer = \
    BigInteger = \
    SmallInteger = \
    DateTime = \
    Float = None

    def nofunc(*args, **kwargs): pass

    Field = \
    TableDef = \
    TableFromModel = nofunc

_dbengine = None

def connectdb():
    global _dbengine
    if not DB_ENABLED:
        raise mdErrors.EStorageError('DB operations are not supported in your environment.')
    if not _dbengine:
        _dbengine = _SQA.create_engine(_getEnv().dbconnstr)
    return _dbengine
