# ###########################################
# Exception~s
# ###########################################

class ECScannerError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)

class EStorageError(ECScannerError):
    pass

class EReportError(ECScannerError):
    pass

class EEnvError(ECScannerError):
    pass