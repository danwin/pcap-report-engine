# ###########################################
# Internal lookup~s (Geo-IP, IANA protocol numbers)
# ###########################################

import csv

try:
    import cPicle as pickle
except ImportError:
    import pickle

from pcapmon import env as mdEnv

_getEnv = mdEnv.get

from pcapmon.monitor import warning, TRACE_EXCEPTIONS

# ###########################################
# IP utils
# ###########################################

o2 = 256
o3 = o2*256
o4 = o3*256

def ip_to_int(s):
    """
    Convert string ip representation to
    integer value:
    >>> ip_to_int('0.0.0.1')
    1
    >>> ip_to_int('0.0.1.0')
    256
    """
    t0, t1, t2, t3 = map(int, s.strip('\'\"').split('.'))
    try:
        return t0 * o4 + t1 * o3 + t2 * o2 + t3
    except ValueError:
        return 0

def ip_to_str(i):
    """
    Convert integer vaue to ip string.
    >>> ip_to_str(ip_to_int('192.168.0.1'))
    '192.168.0.1'
    """
    return "%d.%d.%d.%d" % ((i >> 24) & 255, (i >> 16) & 255, (i >> 8) & 255, i & 255)

def cidr_to_ip(s):
    """
    Convert CIDR notation (string)
    to tuple (net_int, ip_bdcst)
    or ('iplow', 'iphigh')
    >>> map(ip_to_str, cidr_to_ip('10.0.0.0/8'))
    ['10.0.0.0', '10.255.255.255']
    >>> map(ip_to_str, cidr_to_ip('172.16.0.0/12'))
    ['172.16.0.0', '172.31.255.255']
    >>> map(ip_to_str, cidr_to_ip('192.168.0.0/16'))
    ['192.168.0.0', '192.168.255.255']
    """
    ip, mask_cidr = s.split('/')
    ip_int = ip_to_int(ip)
    mask_int = (~0 << (32-int(mask_cidr)))
    net_int = (ip_int & mask_int)
    ip_bdcst = (net_int | ~(mask_int))
    return (net_int, ip_bdcst)


def ip_s_to_ioctets(saddr):
    """
    >>> ip_s_to_ioctets('1.2.3.4')
    [1, 2, 3, 4]
    """
    if saddr.find(':') > -1:
        if not _getEnv().ip6compat:
            return (None, None, None, None)

    return [int(s) for s in saddr.strip('\'\"').split('.')]

def ip_s_next(saddr):
    """
    Find the closest following ip:
    >>> ip_s_next('0.0.255.255')
    '0.1.0.0'
    """
    return ip_to_str(ip_to_int(saddr)+1)

def ip_s_prev(saddr):
    """
    Find the closest previous ip:
    >>> ip_s_prev('0.0.255.0')
    '0.0.254.255'
    """
    v = ip_to_int(saddr)-1
    if v < 0:
        return None
    return ip_to_str(v)

def ip6addr(saddr):
    return saddr.find(':') > -1

# ###########################################
# IPTree
# ###########################################

class IPTree(object):
    """
    Tree-like data storage for IP address~es
    >>> t = IPTree()
    >>> t.append('0.0.0.0', 'zero')
    >>> t.append('1.1.2.1', 'my data')
    >>> t.get('0.0.0.0')
    'zero'
    >>> t.get('1.1.2.1')
    'my data'
    >>> t.get('100.100.100.100')

    """

    def __init__(self):
        self.root = []

    def append(self, saddr, data):
        if ip6addr(saddr):
            return

        tags = ip_s_to_ioctets(saddr)

        level = self.root
        #~ parent = self.root

        #~ print '--->',tags
        for i, o in enumerate(tags):
            #~ print 'I:',i,'level:',level
            if level is None:
                raise Exception('Empty level at: {}; storage: {}'.format(i, type(self.root)))
            if o >= len(level):
                level.extend([None]*(o - len(level) +1))
                #~ level[o] = [] if (i < 3) else data
            if i < 3:
                if level[o] is None:
                    # cerate new list if that level has been expanded before:
                    level[o] = []
                level = level[o]
            else:
                level[o] = data

    def get(self, saddr, default=None):
        if not saddr:
            return
        if ip6addr(saddr):
            return
        tags = ip_s_to_ioctets(saddr)
        level = self.root
        for o in tags:
            if o >= len(level) or not level[o]:
                return default
            level = level[o]
        return level if level is not None else default

    def iterrows(self):
        """
        Return iterator over rows.
        >>> c = IPTree()
        >>> c.append('111.111.111.111', 'host1');
        >>> c.append('222.222.222.222', 'host2');
        >>> for row in c.iterrows(): row
        ('111.111.111.111', 'host1')
        ('222.222.222.222', 'host2')
        """
        for ndx1, lev1 in enumerate(self.root):
            if lev1 is None: continue

            for ndx2, lev2 in enumerate(lev1):
                if lev2 is None: continue

                for ndx3, lev3 in enumerate(lev2):
                    if lev3 is None: continue

                    for ndx4, lev4 in enumerate(lev3):
                        if lev4 is None: continue

                        yield ('{}.{}.{}.{}'.format(ndx1, ndx2, ndx3,ndx4), lev4)


# ###########################################
# IPRanges
# ###########################################

class IPRanges(IPTree):
    """
    Storage for range info string data.
    Used for geoip lookup ip->country
    >>> c = IPRanges()
    >>> c.append('0.0.0.0', 'US')
    >>> c.append('1.0.0.0', 'AU')
    >>> c.append('1.0.1.0', 'CN')
    >>> c.append('1.0.4.0', 'AU')
    >>> c.append('1.0.8.0', 'CN')
    >>> c.append('1.0.16.0', 'JP')
    >>> c.append('1.0.32.0', 'CN')
    >>> c.append('1.0.64.0', 'JP')
    >>> c.append('1.0.128.0',   'TH')
    >>> c.append('1.1.0.0', 'CN')
    >>> c.get('0.0.0.1')
    'US'
    >>> c.get('1.0.17.9')
    'JP'
    >>> c.get('1.1.1.1')
    'CN'
    >>> subnet = ('0.0.0.127','0.0.0.240')
    >>> c.override_range(subnet, 'subnet')
    >>> c.get('0.0.0.126')
    'US'
    >>> c.get('0.0.0.200')
    'subnet'
    >>> c.get('0.0.0.240')
    'subnet'
    >>> c.get('0.0.0.241')
    'US'
    >>> c.append('23.234.190.160', 'US')
    >>> c.append('23.235.64.0', 'CA')
    >>> c.get('23.235.43.223')
    'US'
    """

    payloadtype = str

    def __init__(self, alloclen = 1):
        IPTree.__init__(self)
        self.root = [] * alloclen
        self.allowoverride = True

    def append(self, saddr, data):
        if ip6addr(saddr):
            return
        tag1, tag2, tag3, tag4 = ip_s_to_ioctets(saddr)

        if len(self.root) <= tag1:
            self.root.extend([None]*(tag1 - len(self.root) +1))

        if self.root[tag1] is None:
            self.root[tag1] = {}

        cursor = self.root[tag1]

        for key in (tag2, tag3):
            v = cursor.get(key, None)
            if v is None:
                cursor[key] = {}
            cursor = cursor[key]

        if tag4 in cursor and not self.allowoverride:
            raise Exception('IPRanges error: item "{}" already defined'.format(tag4))

        if not isinstance(data, self.payloadtype):
            raise Exception('IPRanges invalid data type: "{}"'.format(data))

        cursor[tag4] = data

    def override_range(self, snstuple, data):
        """
        Allows to allocate a sub-range inside existing IPRanges data,
        with preserving the formerly associated data for new adjacent
        sub-ranges
        """

        olddata = self.get(snstuple[1])
        self.append(snstuple[0], data)
        if olddata is not None:
            self.append(ip_s_next(snstuple[1]), olddata)


    # ###############################
    def get(self, saddr, default=None, rcount=0):
        if rcount > 10:
            warning('Cannot find record in IPRanges - recursion count is over limit')
            return self.payloadtype(default)

        if not saddr:
            return

        if ip6addr(saddr):
            return

        tags =  ip_s_to_ioctets(saddr)
        tag1, tag2, tag3, tag4 = tags

        if len(self.root) <= tag1 or self.root[tag1] is None:
            return self.payloadtype(default)

        cursor = self.root[tag1]

        for slevel, key in enumerate((tag2, tag3, tag4)):
            v = cursor.get(key, None)
            if v is None:
                akeys = cursor.keys()
                akeys.sort()
                kfound = -1
                for akey in akeys:
                    if akey < key:
                        kfound = akey
                    else:
                        break
                if kfound > -1:
                    v = cursor[kfound]
                else:
                    # search in previous segments:
                    rcount += 1
                    prev_ip = tags[:]
                    if prev_ip[slevel] > 0:
                        prev_ip[slevel] -= 1
                        for i in range(slevel+1, 4):
                            prev_ip[i] = 255
                        prev_ip = '{}.{}.{}.{}'.format(*prev_ip)
                        #~ print('IPRanges: in {} not found, searching at {}'.format(saddr, prev_ip))
                        return self.get(prev_ip, default, rcount+1)
            cursor = v
            if cursor is None:
                break

        return self.payloadtype(cursor if cursor is not None else default)


# ###########################################
# Lookup()
# ###########################################

class Lookup(object):
    """
    Local database to decode value to its' text representation or a label.
    Internally implemented by pandas DataFrame.
    Can be imported from CSV file
    and stored as a pickle.
    (To-do - store in database)?

    """

    fields = []
    header = None

    def _createstorage(self):
        raise NotImplemented(\
            'Lookup._createstorage() must be overriden in descendants.')

    def _set_pd_index(self, _lookup):
        pass

    def _transform(self, _lookup):
        pass

    def __init__(self, label=None, csv_name=None, csv_stream=None, pickle_name=None):
        self.label = label
        self.pickle_name = pickle_name
        self._lookup = self._createstorage()

        if pickle_name:
            # reload data from local storage:
            self._lookup = None
            try:
                # open with a buffered mode (3rd arg == 1)
                with open(self.pickle_name, 'rb', 1) as f:
                    self._lookup = pickle.load(f)
            except IOError:
                self._lookup = None
                warning(\
                    'Internal database for "{}" looks empty, trying to import data from csv...'\
                    .format(self.__class__.__name__))
            except pickle.UnpicklingError as e:
                print('Error in format of internal database.')
                warning('Details: {e}'.format(e))

        # try to import from csv if pickle_name specified but loading fails:
        if self._lookup is None:
            self._lookup = self._createstorage()
            try:
                if csv_name:
                    self.from_csv_file(csv_name)
                    warning('Import for "{}" complete.'.format(self.__class__.__name__))
                    warning('Internal db for {} is up to date now.'.format(self.label))
                else:
                    warning(\
                        'No input storage specified for "{}", creating empty lookup'\
                        .format(self.__class__.__name__))
            except Exception as e:
                print ('Error importing {} database'.format(self.label))
                print (e)
                if TRACE_EXCEPTIONS: raise
                sys.exit(1)

        # load or update _lookup for additional csv_stream if specified:
        if csv_stream:
            self.from_csv_stream(csv_stream)

    def from_csv_stream(self, f):
        """
        Read CSV data from file-like object.
        Must instantiate and return an internal storage
        """
        raise NotImplemented()
        #~ return _lookup

    def from_csv_file(self, fname):
        warning('Reading "{}" lookup data from: {}'.format(self.label, fname))

        try:
            with open(fname, 'rb') as f:
                self.from_csv_stream(f)
        except IOError as e:
            print (\
                "Error reading file '{}' for '{}' database.".format(self.pickle_name, self.label), \
                "Please check access permission for that folder / resource")
            print ("Error info: ", e)
            if TRACE_EXCEPTIONS: raise
            sys.exit(1)

        warning('updating internal "{}" database'.format(self.label))

        self.store()

        warning('Done.')

    def get(self, key):
        raise NotImplemented(self.__class__.__name__+'.get()')

    def __getitem__(self, key):
        return self.get(key)

    def append(self, key, *args):
        raise NotImplemented(self.__class__.__name__+'.append()')

    def store(self):
        try:
            if self.pickle_name:
                with open(self.pickle_name, 'wb') as f:
                    pickle.dump(self._lookup, f, pickle.HIGHEST_PROTOCOL)
        except IOError as e:
            print ("Error writing file '{}' for '{}' database.".format(self.pickle_name, self.label), \
                "Please check access permission for that folder / resource")
            print ("Error info: ", e)
            if TRACE_EXCEPTIONS: raise
            sys.exit(1)


# ###########################################
# GeoIPLookup
# ###########################################

class GeoIPLookup(Lookup):
    """
    Basic IP database (indexed by IP, with meta-info).
    This class implements a range ip lookup fo country.
    >>> from StringIO import StringIO
    >>> data = "'100.0.0.0','100.255.255.255',Country 1\\r\\n"
    >>> data += "'200.0.0.0','200.255.255.255',Country 2\\r\\n"
    >>> c = GeoIPLookup(csv_stream=StringIO(data))
    >>> c.append('107.107.107.1', 'added.com')
    >>> c.get('107.107.107.1')
    'added.com'
    >>> c.get('100.1.1.1')
    'Country 1'
    >>> c.get('200.1.1.1')
    'Country 2'
    >>> c.get('250.1.1.1')
    ''
    >>> c.get('10.1.1.1')
    'local'
    >>> c.get('192.168.0.1')
    'local'
    >>> c.get('220.220.1.1')
    ''
    """

    fields = ['iplow', 'iphigh', 'country']
    header = 0

    def _createstorage(self):
        return IPRanges()

    def __init__(self, label=None, csv_name=None, csv_stream=None, pickle_name=None):
        Lookup.__init__(self, label, csv_name, csv_stream, pickle_name)
        # tune sub-ranges for intranet:
        # standard ranges:
        self._lookup.override_range(('10.0.0.0', '10.255.255.255'), 'local')
        self._lookup.override_range(('172.16.0.0', '172.31.255.255'), 'local')
        self._lookup.override_range(('192.168.0.0', '192.168.255.255'), 'local')
        # local net fom arguments:
        self._lookup.override_range(map(ip_to_str, cidr_to_ip(_getEnv().localnet)), 'local')

    def from_csv_stream(self, f):
        """
        Read CSV data from file-like object.
        Must instantiate and return an internal storage
        """

        ndx_iplow = self.fields.index('iplow')
        ndx_country = self.fields.index('country')

        reader = csv.reader(f)
        for row in reader:
            self._lookup.append(row[ndx_iplow], row[ndx_country])

    def append(self, key, *args):
        """
        Update Geo ip records.
        """
        self._lookup.append(key, args[0])

    def get(self, key):
        return self._lookup.get(key, default='')

# ###########################################
# DNSCache
# ###########################################

class DNSCache(Lookup):
    """
    Cache to hold DNS records:
    ip -> hostname
    >>> c = DNSCache()
    >>> c.append('1.1.1.1', 'test')
    >>> c.append('220.220.10.10', 'www.mydomain.org')
    >>> c.append('220.220.10.10', 'smtp.mydomain.org')
    >>> c.append('70.70.70.1', 'other.com')
    >>> c.get('1.1.1.1')
    'test'
    >>> c.get('70.70.70.1')
    'other.com'
    >>> c.get('220.220.10.10')
    '*.mydomain.org'
    >>> c.get('150.150.150.150')
    ''
    """
    fields = ['ip', 'hostname']
    header = None

    def _createstorage(self):
        return IPTree()

    def from_csv_stream(self, f):
        """
        Read CSV data from file-like object.
        Must instantiate and return an internal storage
        """
        ndx_iplow = self.fields.index('ip')
        ndx_hostname = self.fields.index('hostname')

        reader = csv.reader(f)
        for row in reader:
            self._lookup.append(row[ndx_iplow], row[ndx_hostname])

    def to_csv(self, csv_name):
        """
        Export data to CSV
        """
        with open(csv_name, 'wb') as f:
            writer = csv.writer(f)
            for row in self._lookup.iterrows():
                #~ print('CSV row: ', row)
                writer.writerow(row)

    def to_columns_dict(self):
        col_ip=[]
        col_name = []
        for row in self._lookup.iterrows():
            ip, hostname = row
            col_ip.append(ip)
            col_name.append(hostname)
        result = {}
        result[self.fields[0]] = col_ip
        result[self.fields[1]] = col_name
        return result

    def get(self, saddr):
        return self._lookup.get(saddr, default=saddr) # return name or ip

    def append(self, key, *args):
        """
        Update dns records.
        Find the common tags in a host name.
        >>> c = DNSCache()
        >>> c.append('1.1.1.1', 'mysite.mydomain.com')
        >>> c.get('1.1.1.1')
        'mysite.mydomain.com'
        >>> c.append('1.1.1.1', 'pop3.mydomain.com')
        >>> c.get('1.1.1.1')
        '*.mydomain.com'
        """
        saddr, hostname = key, args[0]
        # check if address exists already:
        olddata = self._lookup.get(saddr, None)
        if olddata:
            if olddata == hostname:
                # the hostname remains the same:
                return
            newtags = hostname.split('.')
            newtags.reverse()
            oldtags = olddata.split('.')
            oldtags.reverse()

            # find common tags:
            newtags = filter(None, map(lambda a, b: a if a==b else None, newtags, oldtags))

            if len(newtags) > 0:
                # add a wildcard to indicate a difference in a sub-domain:
                newtags.append('*')
                # convert to traditional sequence:
                newtags.reverse()
                # render a name string like "*.mydomain.com":
                hostname = '.'.join(newtags)

        self._lookup.append(saddr, hostname)


# ###########################################
# ProtocolNamesLookup
# ###########################################

class ProtocolNamesLookup(Lookup):
    """
    Local DB for IANA names for IP protocols.
    >>> data = ','.join(ProtocolNamesLookup.fields) + "\\r\\n1,Proto 1,,,\\r\\n2,Proto 2,,,"
    >>> from StringIO import StringIO
    >>> c = ProtocolNamesLookup(csv_stream=StringIO(data))
    >>> c.get(1)
    'Proto 1'
    >>> c.get('2')
    'Proto 2'
    >>> c.get(10)
    ''
    """
    fields = ['Decimal','Keyword','Protocol','IPv6 Extension Header','Reference']
    header = 0

    def _createstorage(self):
        return [''] * 256

    def from_csv_stream(self, f):
        """
        Read CSV data from file-like object.
        Must instantiate and return an internal storage
        """
        ndx_code = self.fields.index('Decimal')
        ndx_name = self.fields.index('Keyword')

        reader = csv.reader(f)
        # bypass header:
        reader.next()
        # read rows:
        for row in reader:
            try:
                self._lookup[int(row[ndx_code])] = row[ndx_name]
            except ValueError:
                warning(\
                    'Non-integer key in IANA record: {}'.format(row[ndx_code]))
        #~ print(self._lookup)

    def get(self, key):
        try:
            return self._lookup[int(key)]
        except TypeError:
            warning('Unknown IANA protocol with non-integer code "{}"'.format(key))
            return key
        except IndexError:
            warning('Unknown IANA protocol with code "{}"'.format(key))
            return str(key)


class DictLookup(Lookup):
    """
    Key-value persistent storage.
    Value is a dict where values are access~ible by field name.
    """
    fields = []
    key = None
    header = 1
    
    def _createstorage(self):
        return dict()
        
    def _decode_row_values(self, row):
        "Use the rest of the row fields as a values"
        rec= {}
        for index, value in enumerate(fields):
            if value == self.key:
                continue
            try:
                rec[fields[index]] = row[index]
            except IndexError:
                raise EStorageError( \
                    'Index for lookup "{}" is out of range. \nRequired fields are: "{!r}"'.format(self.label, self.fields))
        return rec
    
    def from_csv_stream(self, f):
        reader = csv.reader(f)
        # bypass header if any:
        if self.header:
            reader.next()
        for row in reader:
            if not row: 
                continue
            self._lookup[row[0].strip(' ')] = self._decode_row_values(row)
        
        #~ with open('lkp.txt', 'wb') as f:
            #~ for key, value in self._lookup.iteritems():
                #~ f.write("'{}':'{}'".format(key, value))
    
    def get(self, key, default = None):
        return self._lookup.get(key, default)

class CountryNameLookup(DictLookup):
    """
    Plain key-value storage with a string values
    """
    fields = ['country', 'latitude' , 'longitude' , 'name']
    key = 'country'
    header = 1

    def _decode_row_values(self, row):
        "Use the 'name' field as value"
        ndx_name = self.fields.index('name')
        if row and len(row) > ndx_name:
            return row[ndx_name] # index of 'name' in row without 'country' key
        else:
            return ''
    
    def get(self, key, default = ''):
        return self._lookup.get(key, default)
