#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# if "env" fails, change the first line to: #!/usr/bin/python
# substitute statement by build-in function
from __future__ import print_function
import os
import fnmatch
import sys

def file_iter(folder_name, *patterns):
    cache = list()
    
    def scan_pattern(rootpath, pattern):
        for root, dirs, files in os.walk(rootpath):
            for filename in fnmatch.filter(files, pattern):
                cache.append(os.path.join(root, filename))

    for pattern in patterns:
        print('Wildcard: "{}"'.format(pattern))
        scan_pattern(rootpath=folder_name, pattern=pattern)
    
    return iter(cache)


def retrieve_input(folder_name):
    print('Scanning folder: {}'.format(folder_name))
    for file_name in file_iter(folder_name, '*.done'):
        # extract file name without '.done' extension:
        initial_name, ext = os.path.splitext(file_name)
        os.rename(file_name, initial_name)
        print ('Renaming {} -> {}'.format(file_name, initial_name))
    print('Done.')

def usage(script_name):
    print(\
    """
    Usage:
    {0} <command> <parameter>.
    
    Supported commands:
        retrieve_input - remove '.done' extension from all files of specified folder (use <parameter> to speciify folder).
    Sample:
        {0} retrieve_input ./samples
        (removes '.done' extension for each file in the "./samples" folder, where "./" means path relative working folder).
    
    """.format(script_name))

if __name__=='__main__':
    # parse args:
    argv = sys.argv
    script_name = os.path.split(argv.pop(0))[1] # bypass script path
    
    if len(argv)>1:
        command = argv.pop(0)
        if command == 'retrieve_input':
            if len(argv)>1:
                parameter = argv.pop(0)
            else:
                parameter = './samples'
            retrieve_input(folder_name=parameter)
            sys.exit(0)
    usage(script_name)
