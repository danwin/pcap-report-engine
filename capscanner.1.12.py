#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# if "env" fails, change the first line to: #!/usr/bin/python
"""
Requirements:
Wireshark version 1.12
You have tshark installed locally
Hint: it is the best to make capture neither on observed client nor on observed server.
"""

import capscanner
import pcapmon.views

# redefine fields for Wireshark 1.12:
# dns.a instead of dns.resp.addr

pcapmon.views.FLD_DNS_RESPONSE = 'dns.a'

if __name__ == '__main__':
	capscanner.main()
